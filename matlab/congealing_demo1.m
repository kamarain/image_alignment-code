%CONGEALING_DEMO1 Congealing simple demo
%
% Generates slightly geometrically transformed copies of the same
% image and then runs the congealing method [1] for them. The plots
% demonstrate the result of the congealing procedure.
%
% NOTE: for some reason the congealing code does not work in
% system() function and therefore you need to run the provided
% shell script every time!!
%
% Authors:
%  Joni Kamarainen, in SGN 2012-2015
%
% Project:
%  LotOfClasses (http://vision.cs.tut.fi/personal/kechen/project/lsvr/index.html)
%
% References:
%  [1] Gary B. Huang, Vidit Jain, and Erik Learned-Miller.
%  Unsupervised joint alignment of complex images.
%  International Conference on Computer Vision (ICCV), 2007.
%  Project Web page: http://vis-www.cs.umass.edu/code/congealingcomplex/

fprintf('---------------------------------------------\n');
fprintf('Simple congealing demo                       \n');
fprintf('---------------------------------------------\n');

fprintf('-- Loading parameters from congealing_demo1_conf.m... --')
run('./congealing_demo1_conf');
fprintf('-- ...Done --\n');

% Check that temp dir exists
if ~exist(conf.temp)
  mkdir(conf.temp);
end;

% Read one example image
fd = mvpr_lopen(conf.imgListFile,'read');
images_list = fullfile(conf.temp,'images.list');
fout = mvpr_lopen(images_list,'write');
images_aligned_list = fullfile(conf.temp,'images_aligned.list');
fout2 = mvpr_lopen(images_aligned_list,'write');
images_model = fullfile(conf.temp,'images.model');

test_images_list = fullfile(conf.temp,'test_images.list');
fout_test = mvpr_lopen(test_images_list,'write');
test_images_aligned_list = fullfile(conf.temp,'test_images_aligned.list');
fout2_test = mvpr_lopen(test_images_aligned_list,'write');

lline = mvpr_lread(fd);
img = imread(fullfile(conf.imgDir,lline{1}));
mvpr_lclose(fd);

t_x =   [0  5 10 20 40 10 10];
t_y =   [0  5 10 20 40 10 10];
theta = [0  0  0  0  0  5 10];
scale = [1  1  1  1  1  1  1];

% Make the test landmark image of R, G and B dots
timg = uint8(zeros(400,400,3));
timg(45:55,45:55,1) = 255;
timg(45:55,95:105,2) = 255;
timg(95:105,95:105,3) = 255;
timg_file = fullfile(conf.temp,'testimage');
imwrite(timg,[timg_file '.bmp']);

% Make transformed images
for tind = 1:length(t_x)
  H = mvpr_h2d_sim(theta(tind),[t_x(tind) t_y(tind)],scale(tind));
  [H_img H_new] = mvpr_imtrans(img,H,'outregion',[0 400 0 400]);  
  subplot(1,2,1);
  imshow(img);
  subplot(1,2,2);
  imshow(H_img);
  [foo fname fext] = fileparts(lline{1});
  imgname = fullfile(conf.temp,...
                         sprintf('%s_%03d%s',fname,tind,fext));
  aligned_imgname = fullfile(conf.temp,...
                             sprintf('%s_%03d_ALIGNED%s',fname,tind,fext));
  imwrite(H_img,imgname);
  mvpr_lwrite(fout,imgname);
  mvpr_lwrite(fout_test,[timg_file '.bmp']);
  mvpr_lwrite(fout2,aligned_imgname);
  mvpr_lwrite(fout2_test,...
              sprintf('%s_%03d_ALIGNED.bmp',timg_file,tind));
  input('<RET>');
end;
mvpr_lclose(fout);
mvpr_lclose(fout2);
mvpr_lclose(fout_test);
mvpr_lclose(fout2_test);

% Run congealing for images
fprintf(['YOU MUST NOW RUN MANUALLY THE SHELL SCRIPT: "source run_congealing.sh"!!'])
input(' -  Press <RET> When done!')

%system(sprintf('%s %s %s', conf.congealReal,...
%               images_list, images_model));
%system(sprintf('%s %s %s %s', conf.funnelReal,...
%       images_list, images_model, images_aligned_list));
%system(sprintf('%s %s %s %s', conf.funnelReal,...
%       test_images_list, images_model, test_images_aligned_list));


% Fetch transformation matrices based on the test image and its
% congealed versions
test_file = mvpr_lopen(test_images_aligned_list,'read');
clf;
subplot(1,2,1);
title('Artificial landmarks');
imagesc(zeros(size(timg)));
hold on;
subplot(1,2,2);
title('After congealing');
imagesc(zeros(size(timg)));
hold on;
for tind = 1:length(t_x)
  lline = mvpr_lread(test_file);
  timg_c = imread(lline{1});
  
  [r_y r_x r_val] = find(squeeze(timg_c(:,:,1)) > 10);
  r_y = mean(r_y); r_x = mean(r_x);
  [g_y g_x g_val] = find(squeeze(timg_c(:,:,2)) > 10);
  g_y = mean(g_y); g_x = mean(g_x);
  [b_y b_x b_val] = find(squeeze(timg_c(:,:,3)) > 10);
  b_y = mean(b_y); b_x = mean(b_x);
  Hest = mvpr_h2d_corresp([r_x r_y; g_x g_y; b_x b_y]',...
                          [50 50; 100 50; 100 100]',...
                          'hType','similarity');
  
  H = mvpr_h2d_sim(theta(tind),[t_x(tind) t_y(tind)],scale(tind));
  [H_timg H_new] = mvpr_imtrans(timg,H,'outregion',[0 400 0 400]);  
  [H_r_y H_r_x H_r_val] = find(squeeze(H_timg(:,:,1)) > 10);
  H_r_y = mean(H_r_y); H_r_x = mean(H_r_x);
  [H_g_y H_g_x H_g_val] = find(squeeze(H_timg(:,:,2)) > 10);
  H_g_y = mean(H_g_y); H_g_x = mean(H_g_x);
  [H_b_y H_b_x H_b_val] = find(squeeze(H_timg(:,:,3)) > 10);
  H_b_y = mean(H_b_y); H_b_x = mean(H_b_x);
  % Plot distorted
  subplot(1,2,1);
  plot(H_r_x,H_r_y,'rd','MarkerSize',14,'LineWidth',2);
  plot(H_g_x,H_g_y,'gd','MarkerSize',11,'LineWidth',2);
  plot(H_b_x,H_b_y,'bd','MarkerSize',8,'LineWidth',2);
  % Plot "corrected" by congealing
  subplot(1,2,2);
  plot(r_x,r_y,'ro','MarkerSize',14,'LineWidth',2);
  plot(g_x,g_y,'go','MarkerSize',11,'LineWidth',2);
  plot(b_x,b_y,'bo','MarkerSize',8,'LineWidth',2);
end;
