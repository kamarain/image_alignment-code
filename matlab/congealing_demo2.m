%DEMO2 Congealing for an image ensemble
%
% Runs the congealing algorithm from ref. [1] for a given set of
% images similar to IMAGE_ALIGNMENT_DEMO2.M .
%
% NOTE: for some reason the congealing code does not work in
% system() function and therefore you need to run the provided
% shell script every time!!
%
% Authors:
%  Joni Kamarainen, in SGN 2012-2015
%
% Project:
%  LotOfClasses (http://vision.cs.tut.fi/personal/kechen/project/lsvr/index.html)
%
% References:
%  [1] Gary B. Huang, Vidit Jain, and Erik Learned-Miller.
%  Unsupervised joint alignment of complex images.
%  International Conference on Computer Vision (ICCV), 2007.
%  Project Web page: http://vis-www.cs.umass.edu/code/congealingcomplex/

fprintf('---------------------------------------------\n');
fprintf('Caltech-101 congealing                       \n');
fprintf('---------------------------------------------\n');

fprintf('-- Loading parameters from congealing_demo2_conf.m... --')
run('./congealing_demo2_conf');
fprintf('-- ...Done --\n');

% Check that temp dir exists
if ~exist(conf.temp)
  mkdir(conf.temp);
end;

% Make <classname>_images.list file and
% <classname>_images_aligned.list files for the congealing
% executable and give name to <classname>.model
totNum = mvpr_lcountentries(conf.imgListFile);
files_fd = mvpr_lopen(conf.imgListFile,'read');
images_list_file = ...
    fullfile(conf.temp,[conf.classes{conf.classno} '_images.list']);
images_list_fd = mvpr_lopen(images_list_file, 'write');
images_aligned_list_file = ...
    fullfile(conf.temp, [conf.classes{conf.classno} '_images_aligned.list']);
images_aligned_list_fd = mvpr_lopen(images_aligned_list_file,'write');
model_file = ...
    fullfile(conf.temp, [conf.classes{conf.classno} '_images.model']);

for lineNo = 1:totNum
  lline = mvpr_lread(files_fd);
  mvpr_lwrite(images_list_fd,...
              fullfile(conf.imgDir,lline{1}));
  [fpath fname fext] = fileparts(lline{1});
  mvpr_lwrite(images_aligned_list_fd,...
              fullfile(conf.temp,...
                       [conf.classes{conf.classno} '_' fname '_aligned' fext]));
end;
mvpr_lclose(files_fd);
mvpr_lclose(images_list_fd);
mvpr_lclose(images_aligned_list_fd);

% Make a dummy test image that is used to find the transformations
files_fd = mvpr_lopen(conf.imgListFile,'read');
dummy_images_list_file = ...
    fullfile(conf.temp,[conf.classes{conf.classno} '_images_dummy.list']);
dummy_images_list_fd = mvpr_lopen(dummy_images_list_file,'write');
dummy_images_aligned_list_file = ...
    fullfile(conf.temp,[conf.classes{conf.classno} '_images_aligned_dummy.list']);
dummy_images_aligned_list_fd = mvpr_lopen(dummy_images_aligned_list_file,...
                                          'write');

for lineNo = 1:totNum
  lline = mvpr_lread(files_fd);
  mvpr_lwrite(dummy_images_list_fd,...
              fullfile(conf.temp,'dummy_image.bmp'));
  [fpath fname fext] = fileparts(lline{1});
  mvpr_lwrite(images_aligned_list_fd,...
              fullfile(conf.temp,...
                       [conf.classes{conf.classno} '_' fname '_aligned_dummy.bmp']));
end;
mvpr_lclose(files_fd);
mvpr_lclose(dummy_images_list_fd);
mvpr_lclose(dummy_images_aligned_list_fd);

% Make the actual dummy image with at least artificial landmarks
% that can be automatically found
dummyimg = uint8(zeros(400,400,3));
dummyimg(45:55,45:55,1) = 255;
dummyimg(45:55,95:105,2) = 255;
dummyimg(95:105,95:105,3) = 255;
dummyimg_file = fullfile(conf.temp,'dummy_image.bmp');
imwrite(dummyimg,dummyimg_file);

% Run congealing for images (needs to be done manually in the shell)
fprintf(['!!!! YOU MUST NOW RUN MANUALLY THE SHELL SCRIPT !!!!\n']);
fprintf(['Copy paste the following commands to your shell in the '...
        'matlab/congealing directory:\n'])
fprintf([' COMMAND 1: ' conf.congealReal ' ' images_list_file ' ' model_file '\n']);
fprintf([' COMMAND 2: ' conf.funnelReal ' ' images_list_file ' ' model_file ' ' ...
         images_aligned_list_file '\n']);
fprintf([' COMMAND 3: ' conf.funnelReal ' ' dummy_images_list_file ' ' model_file ' ' ...
         dummy_images_aligned_list_file '\n']);
input(['!!!!  PRESS <RETURN> WHEN DONE !!!!\n'])

%
% For evaluation, we need the similarity transformation matrices
% and this is achieved by recovering transformations using the
% dummy image and its transformations

test_file = mvpr_lopen(dummy_images_aligned_list_file,'read');
%clf;
%hold on;
for tind = 1:totNum
  lline = mvpr_lread(test_file);
  timg_c = imread(lline{1});
  
  % Find (avg) locations of the artificial landmarks
  [r_y r_x r_val] = find(squeeze(timg_c(:,:,1)) > 10);
  r_y(tind) = mean(r_y); r_x(tind) = mean(r_x);
  [g_y g_x g_val] = find(squeeze(timg_c(:,:,2)) > 10);
  g_y(tind) = mean(g_y); g_x(tind) = mean(g_x);
  [b_y b_x b_val] = find(squeeze(timg_c(:,:,3)) > 10);
  b_y(tind) = mean(b_y); b_x(tind) = mean(b_x);
  %plot(r_x(tind),r_y(tind),'ro','MarkerSize',14,'LineWidth',2);
  %plot(g_x(tind),g_y(tind),'go','MarkerSize',11,'LineWidth',2);
  %plot(b_x(tind),b_y(tind),'bo','MarkerSize',8,'LineWidth',2);
end;
%hold off;
mvpr_lclose(test_file);

%
% Test each image as the seed and select the best
test_file = mvpr_lopen(dummy_images_aligned_list_file,'read');
for tind = 1:totNum
  lline = mvpr_lread(test_file);
  timg_c = imread(lline{1});
  siS.seedImgNo = tind;
  % Normalise transformations to the computed seed
  for Hind = 1:totNum
    lmT{Hind} = mvpr_h2d_corresp(...
        [r_x(Hind) r_y(Hind); g_x(Hind) g_y(Hind); b_x(Hind) b_y(Hind)]',...
        [r_x(tind) r_y(tind); g_x(tind) g_y(tind); b_x(tind) b_y(tind)]',...
        'hType','similarity');
  end;
  % Use the same scoring function as our method does
  [lmT lmScores] = eval_aligned_landmarks(siS, lmT,...
                                               conf.imgListFile,...
                                               conf.gtListFile,...
                                               [],...
                                               'imgDir',conf.imgDir,...
                                               'gtDir',conf.gtDir);

  % Plot error curve
  fh = figure(669);
  set(fh,'Visible', 'on'); hold on;
  plot(sort(cell2mat([lmScores]),2),[0:(length(lmScores)-1)],'LineWidth',2); 
  legend('Automatic landmarks');
  axis([0 0.5 0 length(lmScores)]);
  grid on;
  title('landmark error');
  xlabel('Error (relative)');
  ylabel('Number of fitted landmarks');
  if (conf.storeResults)
    acc_graph = sort(cell2mat([lmScores]),2);
    acc_graph = [1:size(acc_graph,2); acc_graph]';
    res_save_file = [cell2mat(conf.classes(conf.classno))...
                     '_seedno_' num2str(siS.seedImgNo)...
                     '.txt'];
    save(fullfile(conf.temp,res_save_file), 'acc_graph','-ascii');
    set(fh,'color','white');drawnow;
    %export_fig(fh);
  end;
  
  % Plot average image
  
  
  
  % Print some evaluation
  %resScores = sort(cell2mat([lmScores; gtScores]),2);
  resScores = sort(cell2mat([lmScores]),2);
  fprintf('Seedno-%3d: ',tind);
  for thind = 1:length(conf.resThresholds);
    if (max(squeeze(resScores(1,:))) <= conf.resThresholds(thind))
      res(tind, thind) = size(resScores,2);
    else
      res(tind, thind) = ...
          round(interp1(squeeze(resScores(1,:)),1:size(resScores,2),conf.resThresholds(thind),'linear'));
    end;
    fprintf('%3d (%2.1f%%) for th=%1.3f, ',res(tind,thind), ...
            res(tind,thind)/totNum*100, conf.resThresholds(thind));
  end;
  fprintf('\n');
end;
fprintf(['Above are the numbers of correctly aligned images for ' ...
         ' different accuracy thresholds and different "seeds". ' ...
         'The bigger the better.\n']);
mvpr_lclose(test_file);

[fh avgImg] = show_aligned_average_image(images_aligned_list_file,...
                                         [], [], 'tempDir', [],...
                                         'imgDir', './','noAlignment', true, 'isVisible',false);
if (conf.storeResults)
  plotfigh = figure(666); imshow(avgImg); set(plotfigh,'color','white');drawnow;
  export_fig(fullfile(conf.temp,...
                      [cell2mat(conf.classes(conf.classno)) ...
                      '_congealed_average.png']));
end;

      
files_fd = mvpr_lopen(conf.imgListFile,'read');
for lineNo = 1:totNum
  lline = mvpr_lread(files_fd);
  [fpath fname fext] = fileparts(lline{1});

  aligned_img = ...
      fullfile(conf.temp,...
               [cell2mat(conf.classes(conf.classno)) '_' fname '_aligned.jpg']);
  
end;
mvpr_lclose(files_fd);
