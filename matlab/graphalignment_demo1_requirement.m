%REPORT02_DEMO2 Automatic object alignment ((r-)Caltech-10)
%
% This is basically a copy of base/bmvc2011_demo.m - only additional
% classes added.
%
% This demo runs the alignment algorithm described in ref. [1] and
% plots qualitative results (average of aligned images) and
% quantitative results (using ground truth manually annotated
% landmarks). This demo was used to search for the best combination
% of parameter values.
%
% Authors:
%  Jukka Lankinen, in MVPR 2011-2012
%  Joni Kamarainen, in MVPR 2011-2012
%
% Project:
%  Object3D2D (http://www2.it.lut.fi/project/object3d2d/index.shtml)
%
% References:
%  [1] J. Lankinen and J.-K. Kamarainen, Local Feature Based
%  Unsupervised Alignment of Object Class Images, British Machine
%  Vision Conference (BMVC2011).
%  [2] J. Lankinen and J.-K. Kamarainen, Unsupervised Alignment of
%  Visual Class Examples, submitted, 2012.


fprintf('---------------------------------------------\n');
fprintf('Unsupervised Visual Alighnment with Similarity Graphs  \n');
fprintf('using the data sets in our CVPR2015 paper    \n');
fprintf('---------------------------------------------\n');

fprintf('-- Loading parameters from Image_alignment_simGraph_demo1_conf1... --')
run('./graphalignment_demo1_conf1');
fprintf('-- ...Done --\n');

% these line is just for LFW5000 and for other cases should be commented.
%num1 = input('Enter starting of the scope: ');
%num2 = input('Enter ending of the scope: ');

fprintf('-- computimg similarity for each par of images --\n')
[a b c] = direct_alignment(conf.seed, conf.imgListFile, ...  %num1, num2 should be added in case of testing lfw dataset 
                                  'seedMethod',conf.seedMethod,...
                                  'imgDir', conf.imgDir, ...
                                  'tempDir', conf.tempDir,...
                                  'enforceFeatureExtract', conf.enforceFeatureExtract,...
                                  'enforceFeaturePostProc', conf.enforceFeaturePostProc,...
                                  'enforceSeedDistmatrices',conf.enforceSeedDistmatrices,...
                                  'enforceSpatScoreMatrices',conf.enforceSpatScoreMatrices,...
                                  'enforceLMMatch', conf.enforceLMMatch,...
                                  'gtDir', conf.gtDir, ...
                                  'gtListFile', conf.gtListFile,...
                                  'debugLevel', conf.debugLevel,...
				                  'showResults', conf.showResults,...
				                  'showBBResults', conf.showResults,...
				                  'storeResults', conf.storeResults,...
                                  'ip_method', conf.ip_method,...
                                  'useBB', conf.useBB,...
                                  'bbDir',conf.bbDir,...
                                  'spatMatchingMethod', conf.spatMatchingMethod);
fprintf('-- ...Done --\n');
fclose all
