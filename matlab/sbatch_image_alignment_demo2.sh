#!/bin/bash
#
# RUN: sbatch --array=15-23:1 sbatch_image_alignment_demo2.sh
# NOTE: for most --mem=4096 is fine but some need --mem=20480
#
# At first let's give job some descriptive name to distinct
# from other jobs running on cluster. Note that %a will have
# the same value as $SLURM_ARRAY_TASK_ID later.
#
#SBATCH -J align-%a
#
# Let's redirect job's out some other file than default slurm-%jobid-out
#SBATCH --output=sbatch_image_alignment_demo2-%a.log
#SBATCH --error=sbatch_image_alignment_demo2-%a.err
#
# We'll want to allocate one CPU core
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#
# We'll want to reserve 2GB memory for the job
# and 3 days of compute time to finish.
#
#SBATCH --mem=8182
#SBATCH --time=10:00:00
#SBATCH --partition=normal
#
# These commands will be executed on the compute node:

module load matlab
cd /home/kamarain/Work/bitbucket/image_alignment-code/matlab
matlab -singleCompThread -nojvm -nodisplay -nosplash -r "sbatch_classno=$SLURM_ARRAY_TASK_ID; sbatch_image_alignment_demo2"