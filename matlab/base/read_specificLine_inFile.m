%the function reads a line by getting file directory and the intended 
%line number which needs to be read

%by fatemeh shokrollahi


function [ filepair ] = read_specificLine_inFile( file,lineNum )
%reads a line of a file by getting it's line number in the given file.

    fh = mvpr_lopen(file, 'read');
    img =  mvpr_lread(fh);  
    offset = ftell(fh.fid); 
    fidPos = offset * (lineNum - 1);
    fseek(fh.fid, fidPos,'bof'); 
    filepair =  mvpr_lread(fh);
      
%      [Dir Name Ext] = fileparts(filepair{1}); 
      
     mvpr_lclose(fh);

end 

