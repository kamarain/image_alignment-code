% Calculates overlaps of bounding boxes after the alignment
% procedure (the better the overlap, the better the alignment, presumbly).

function [bbOverlaps bbCoords] = eval_aligned_boundingbox(siS_, T_, imgListFile_, ...
                                               featListFile_,imgTot_,varargin)

% Parse input arguments
conf = struct('tempDir', 'temp/',...
              'imgDir', './',...
              'bbDir', './',...
              'debugLevel', 0);

conf = mvpr_getargs(conf,varargin);

% Load the seed bounding box
fh = mvpr_lopen(imgListFile_,'read');
for lineNo = 1:siS_.seedImgNo
  fline = mvpr_lread(fh);
end;
mvpr_lclose(fh);
seedBB = load(fullfile(conf.bbDir,fline{2}));
if isfield(seedBB,'box_coord') % Caltech-101 style
  seed_box_coord = seedBB.box_coord;
else % assuming full box corners given as row: tl, tr, br, bl
     % form minY maxY minX maxX ImageNet style
  seed_box_coord = [seedBB(1,2) seedBB(3,2) seedBB(1,1) seedBB(3,1)];
end;

% Load the true bounding boxes, transform the seed bounding box
% using the estimated transformation and compute overlap

% just to make sure that in order
[sortImgTot sortInd] = sort(imgTot_);
imgTot = imgTot_(sortInd);
T = T_(sortInd);
totNum = mvpr_lcountentries(imgListFile_);
fh = mvpr_lopen(imgListFile_,'read');
box_coord = [];
for lineNo = 1:totNum
  fline = mvpr_lread(fh);
  if sum(lineNo == imgTot)
    BB = load(fullfile(conf.bbDir,fline{2}));
    if isfield(BB,'box_coord') % Caltech-101 style
      box_coord = [box_coord; BB.box_coord];
    else % assuming full box corners given as row: tl, tr, br, bl
         % form minY maxY minX maxX ImageNet style
      box_coord = [box_coord; BB(1,2) BB(3,2) BB(1,1) BB(3,1)];
    end;
    if (conf.debugLevel > 1)
      debugImgs{find(lineNo == imgTot)} = fline{1};
    end;
  end;
end;
mvpr_lclose(fh);

for tInd = 1:length(T_)
  p = [seed_box_coord(3) seed_box_coord(4);
       seed_box_coord(1) seed_box_coord(2)];
  p_T = mvpr_h2d_trans(p,T{tInd});
  T_box_coord(tInd,:) = [p_T(2,1) p_T(2,2) p_T(1,1) p_T(1,2)];

  % Compute overlap area
  gt_boxcor_x = [box_coord(tInd,3) box_coord(tInd,4) box_coord(tInd,4) ...
                 box_coord(tInd,3) box_coord(tInd,3)];
  gt_boxcor_y = [box_coord(tInd,1) box_coord(tInd,1) box_coord(tInd,2) ...
                 box_coord(tInd,2) box_coord(tInd,1)];
  T_boxcor_x = [T_box_coord(tInd,3) T_box_coord(tInd,4) T_box_coord(tInd,4) ...
                T_box_coord(tInd,3) T_box_coord(tInd,3)];
  T_boxcor_y = [T_box_coord(tInd,1) T_box_coord(tInd,1) T_box_coord(tInd,2) ...
                T_box_coord(tInd,2) T_box_coord(tInd,1)];
  maxX = ceil(max([gt_boxcor_x T_boxcor_x]));
  maxY = ceil(max([gt_boxcor_y T_boxcor_y]));
  gt_mask = poly2mask(gt_boxcor_x,gt_boxcor_y,max([maxX maxY]),max([maxX ...
                   maxY]));
  T_mask  = poly2mask(T_boxcor_x,T_boxcor_y,max([maxX maxY]),max([maxX ...
                   maxY]));
  %[int_x,int_y] = curveintersect(gt_boxcor_x,gt_boxcor_y,T_boxcor_x,T_boxcor_y);
  int_mask = gt_mask & T_mask;
  bbOverlaps(tInd) = sum(int_mask(:))/max([sum(gt_mask(:)) sum(T_mask(:))]);
  
  if conf.debugLevel > 1
    dbg_img = imread(fullfile(conf.imgDir,debugImgs{tInd}));
    imagesc(dbg_img);
    if (ndims(dbg_img) < 3)
      colormap gray;
    end;
    hold on;
    % rectangle clock-wise from the upper left corner 
    plot([box_coord(tInd,3) box_coord(tInd,4) box_coord(tInd,4) box_coord(tInd,3) box_coord(tInd,3)],...
         [box_coord(tInd,1) box_coord(tInd,1) box_coord(tInd,2) box_coord(tInd,2) box_coord(tInd,1)],...
         'r-','LineWidth',4)
    plot([T_box_coord(tInd,3) T_box_coord(tInd,4) T_box_coord(tInd,4) T_box_coord(tInd,3) T_box_coord(tInd,3)],...
         [T_box_coord(tInd,1) T_box_coord(tInd,1) T_box_coord(tInd,2) T_box_coord(tInd,2) T_box_coord(tInd,1)],...
         'g--','LineWidth',4)
    hold off;
    fprintf('Area overlap: %2.1f percent ',bbOverlaps(tInd)*100);
    input('<RET>');
  end;
end;

if nargout > 1
  bbCoords = T_box_coord;
end;
