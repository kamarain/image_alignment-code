% Selects the seed image for automatic landmark selection
%
% [siS] = o3d2d_autolm_select_seed_files(imgListFile) selects a seed image for 
% automatic landmark selection and returns Seed Image Structure siS. This 
% structure has following information about the seed image:
%     
%
% Please note that this function selects a RANDOM seed by default. 
% This function also requires extracted image features in temp folder by 
% MVPR_FEATURE_EXTRACT_FILES function.
%
% Inputs:
%  imgListFile -  A list of images to be processed, one per line,
%                 containing no white spaces (this cuts the name) and
%                 with their full path (if 'imgDir' not specified).
%
% <Optional>
% 'method'       - Selection method:
%                  'random', 'forced', 'heuristic' (Default: 'random')
% 'tempDir'      - Directory for temporary items
%                  (e.g. extracted descriptors) (Def: 'temp/')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgListFile) (Def: '/')
% 'debugLevel'   - Debug level [0,1,2] (Def: 0 = none)
% 'forceSeed'    - Use this image number or a string containing the
%                  file name without path and extension (Def. [])
%
% Outputs:
%  siS           - Seed image structure.
%
% References:
%
% See also: MVPR_FEATURE_EXTRACT_FILES
%
function siS = o3d2d_autolm_select_seed_files(imgListFile, varargin)

%
% Parse input arguments
conf = struct('method','random',...
              'imgDir', '.',...
              'featListFile', [],...
              'forceSeed', 1,...
              'tempDir', 'TEMPWORK',...
              'debugLevel', 0);              
conf = mvpr_getargs(conf,varargin);

% The default is the same as in MVPR_FEATURE_EXTRACT_FILES
if isempty(conf.featListFile)
    [fpath fname fext] = fileparts(imgListFile);
    featListFile = fullfile(conf.tempDir,[fname '_feat' fext]);
else
    featListFile = conf.featListFile;
end;

if (~exist(featListFile))
    error(['List file ' featListFile 'containing images and extracted local'...
           'features not found - run mvpr_feature_extract_files() for ' ...
           'your images first!']);
end;

% forced selection
siS.seedImgNo = 0;
if (strcmp(conf.method, 'forced'))
    imgTot = mvpr_lcountentries(featListFile);
    fh = mvpr_lopen(featListFile,'read');
    for imgInd = 1:imgTot
        fline = mvpr_lread(fh);
        [foo filebody fileext] = fileparts(fline{2});
        if (isnumeric(conf.forceSeed))
            if imgInd == conf.forceSeed
                siS.seedImgNo = imgInd;
                seedImg = fline{2};
                seedFeats = fline{3}; 
                break;
            end;
        else
            if (strcmp(filebody, conf.forceSeed)) % imgname
                siS.seedImgNo = imgInd;
                seedImg = fline{2};
                seedFeats = fline{3}; 
                break;
            elseif (strcmp([filebody fileext], conf.forceSeed)) % imgname.ext
                siS.seedImgNo = imgInd;
                seedImg = fline{2};
                seedFeats = fline{3}; 
                break;
            elseif (strcmp(fline{2}, conf.forceSeed)) % full path&name
                siS.seedImgNo = imgInd;
                seedImg = fline{2};
                seedFeats = fline{3}; 
                break;				
            end;
        end;
    end;
    mvpr_lclose(fh);
    if (siS.seedImgNo == 0)
        error('Enforced seed not found - check the name/number!');
    end
elseif (strcmp(conf.method, 'random')) % random selection
    imgTot = mvpr_lcountentries(featListFile);
    foo = randperm(imgTot);
    siS.seedImgNo = foo(1);

    fh = mvpr_lopen(featListFile,'read');
    for imgInd = 1:siS.seedImgNo
        fline = mvpr_lread(fh);
        seedImg = fline{2};
        seedFeats = fline{3}; 
    end;
    mvpr_lclose(fh);
else
    error(['Uknown method: ' conf.method]);
end;

% Load image features and generate a "seed structure"
load(fullfile(conf.tempDir,seedFeats),'descriptors','frames');
siS.seedFeatFile = seedFeats;
siS.seedImgFile = seedImg;
siS.descriptors = descriptors;
siS.frames = frames;
[foo fname] = fileparts(seedFeats);
siS.seedID = regexprep(fname, filesep, '_');
seed_img = imread(fullfile(conf.imgDir,seedImg));
siS.norm = norm([size(seed_img,1) size(seed_img,2)]);

return; % remaining is old and deprecated code

%
% Count and validate input images

imgInd = 0; % Image index (for iterations)
filelist = {}; % List of imagefiles

fh = mvpr_lopen(imgFile_, 'read');
filepair = mvpr_lread(fh);
while ~isempty(filepair)
	imgTot = imgTot + 1;
	filelist{imgTot} = filepair{1}; % populate the list of images
	filepair = mvpr_lread(fh);
end
mvpr_lclose(fh);

%
% Seed selection
siS.method = conf.method;
siS.seedImgNo = 0;

if conf.debugLevel > 0 
	fprintf('Selecting seed image\n');
end

%
% random selection
%
%elseif (strcmp(conf.method, 'random'))
%	siS.seedImgNo = ceil(imgTot.*rand(1));
if (strcmp(conf.method, 'random'))
	siS.seedImgNo = ceil(imgTot.*rand(1));

%
% heuristic selection
%  - Basically selects a seed based on random number of comparisons between images
%  - Selects the seed with the highest amount of matches
%
elseif (strcmp(conf.method, 'heuristic'))
	%
	% Check required functionality
	if (exist('mvpr_feature_extract') == 0)
		error(['MVPR matlab library required for calculation'])
	end
	
	totalScores = zeros(size(filelist,1));
	for filen = 1:size(filelist,1)
		% Load descriptors
		[imgDir imgName imgExt] = fileparts(filelist{filen});
		imgFeatFile = [fullfile(conf.tempDir,imgDir,imgName) '_features.mat'];
		load(imgFeatFile,'descriptors','frames');
		
		% Set seed localfeatures 
		seed.lf = lf;
		seed.descriptorHits = [];
		descriptorHits = zeros(size(lf,2));
		for filem=1:20
			n = floor(rand * size(filelist, 1)) + 1;
			
			% Load descriptors
			[imgDir imgName imgExt] = fileparts(filelist{n});
			imgFeatFile = [fullfile(conf.tempDir,imgDir,imgName)...
				       '_features.mat'];
			load(imgFeatFile,'descriptors','frames');

			% Calculate matches
			matches = vl_ubcmatch(seed.descriptors, descriptors);
			
			for j = 1:size(matches, 2)
				descriptorHits(matches(1,j)) = descriptorHits(matches(1,j)) + 1;
			end
		end
		[Y I] = sort(descriptorHits, 'descend');
		totalScores(filen) = Y(1) + Y(2) + Y(3) + Y(4) + Y(5);

		if conf.debugLevel > 0
			fprintf('\r  * Image (%5d/%5d) Score (%3d)', filen, imgTot, totalScores(filen));
		end
	end
	
	[Y I] = sort(totalScores, 'descend');

	if conf.debugLevel > 0
		fprintf('\n  * Best sample selected by heuristics: %s\n', filelist{I(1)});
	end

	siS.seedImgNo = I(1);
else
	error('Unknown ''method''.');
end % end of method selection
if conf.debugLevel > 0
	fprintf('  * Selected seed image: %s (%d)\n', filelist{siS.seedImgNo}, siS.seedImgNo);
end


