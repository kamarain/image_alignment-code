% Plot an average image
%
% [fh avgImg] = o3d2d_autolm_plot_avg_files(imgListFile, siS)
%
% Goes through the images in imgListFile, loads images and landmarks
% and transformation, transform each image to the seed space using
% the transformation and finally plots and returns average image of
% them all.
%
% Inputs:
%  imgListFile - A list of images to be processed, one per line,
%                containing no white spaces (this cuts the name) and
%                with their full path (if 'imgDir' not specified).
%  siS         - Seed image structure.
%  
%
% <Optional>
% 'avgImgSize'   - [width height] if given, the average image will
%                  be of this size, otherwise 120% of median size
%                  of all images (Def. [])
% 'tempDir'      - Directory for temporary save items
%                  (e.g. extracted descriptors) (Def: 'temp/')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgListFile) (Def: 'images/')
% 'saveExt'      - Extension to be added landmark files:
%                  <TEMPSAVE><IMGDIR>_landmark_<SAVEEXT>.txt
% 'isVisible'    - Is figure visible by default? (Def: true)
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
% 'debugInteractive' - Enable interactive mode in debug (Def: true)
%
% Outputs:
%  fh		 - Figure handle to the generated figure
%  avgImg        - The generated average image
%
% References:
%
% See also:
%
function [fh avgImg] = show_aligned_average_image(imgFile_, siS_, scores, varargin)

%
% Parse input arguments
conf = struct('avgImgSize', [],...
              'crop', [],...
              'saveExt', [],...
              'tempDir', 'temp/',...
              'imgDir', 'images/',...
              'gtDir', '/',...
              'debugLevel', 0,...
              'debugInteractive', true,...
              'isVisible', true,...
              'noAlignment', false, ...
              'useIdeal', false,...
              'useBest', false);
conf = mvpr_getargs(conf,varargin);

% Count number of images
imgTot = mvpr_lcountentries(imgFile_);

if isempty(conf.avgImgSize )
  % count the mean size of images
  szI = zeros(imgTot,1);
  szJ = zeros(imgTot,1);
  fh = mvpr_lopen(imgFile_, 'read');
  for imgInd = 1:imgTot
    filepair = mvpr_lread(fh);
    if (length(filepair) == 1) % assuming just a list of images
      img = imread(fullfile(conf.imgDir,filepair{1}));
    else % assuming a feature list produced by our methods
      img = imread(fullfile(conf.imgDir,filepair{2}));
    end;
    szI(imgInd) = size(img,1);
    szJ(imgInd) = size(img,2);
  end;
  mvpr_lclose(fh);
  avgImgSize = [round(median(szJ,1)*1.2) round(median(szI,1)*1.2)]
else
  avgImgSize = conf.avgImgSize;
end;

% Main loop:
%  Go through images and find the best transformation
fh = mvpr_lopen(imgFile_, 'read');



% Creating the matrix to be plotted
avgImg = zeros(avgImgSize(2), avgImgSize(1),3);
avgImgEye = zeros(avgImgSize(2), avgImgSize(1),3);
avgOrigo = round([avgImgSize(2)/2 avgImgSize(1)/2]);

% Main loop:
%  Go through images and find the best transformation
fh = mvpr_lopen(imgFile_, 'read');

if ~conf.isVisible
	figh = figure('visible','off');
else
	figh = figure; 
end
hold all;

fprintf('Creating an average plot\n');

for imgInd = 1:imgTot
	if conf.noAlignment == true
		filepair = mvpr_lread(fh);
                if (length(filepair) == 1) % assuming just a list of images
                  img = imread(fullfile(conf.imgDir,filepair{1}));
                else % assuming a feature list produced by our methods
                  img = imread(fullfile(conf.imgDir,filepair{2}));
                end;

                if ndims(img) < 3 % enforce RGB
                  img = repmat(img, [1 1 3]);
                end;
                  
                
		d = floor(([size(avgImg,1) size(avgImg,2)]-...
                           [size(img,1) size(img,2)])/2);
	
		posX = 1;
		posY = 1;
	
		startX = 1;
		startY = 1;
	
		if d(1) > 0
			posY = d(1);
		else
			startY = -d(1);
		end
		if d(2) > 0
			posX = d(2);
		else
			startX = -d(2);
		end
		if posX == 0
			posX = 1;
		end
		if posY == 0
			posY = 1;
		end
		if startX == 0
			startX = 1;
		end		
		if startY == 0
			startY = 1;
		end			
		clippedImg = img(startY:size(img,1)-startY-1, startX:size(img,2)-startX-1,:);
		clippedImg = double(clippedImg);	
		
		avgImg(posY:size(clippedImg,1)+posY-1, posX:size(clippedImg,2)+posX-1,:) = ...
			   avgImg(posY:size(clippedImg,1)+posY-1, posX:size(clippedImg,2)+posX-1,:) + ...
			   clippedImg;	
	else
		filepair = mvpr_lread(fh);

		% Load landmarks 
		[imgDir imgName imgExt] = fileparts(filepair{2});
		if (isempty(conf.saveExt))
			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
			'_' siS_.seedID '_landmarks.mat']; 
		else
			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
			'_' siS_.seedID '_landmarks_' conf.saveExt '.mat']; 
		end	
		load(landmarks_SaveFile,'Tseed','T','siS', 'tpointS');
	
		% Load image
		img = imread(fullfile(conf.imgDir,filepair{2}));

                if ndims(img) < 3 % enforce RGB
                  img = repmat(img, [1 1 3]);
                end;

		% Compose average image
		congImg = mvpr_imcompose(size(avgImg), avgOrigo, img, T(:,:,1));
		avgImg = avgImg+double(congImg);
		avgImgEye = avgImgEye+double(mvpr_imcompose(size(avgImg),avgOrigo,img,eye(3)));


		%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
		if (conf.debugLevel >= 1)
			if (exist('dbgFig1') == false)
				clf;
				dbgFig1 = gcf;
				dbgAxis1 = axes('Parent',dbgFig1);
				dbgFig2 = figure;
				dbgAxis2 = axes('Parent',dbgFig2);
				dbgFig3 = figure;
				dbgAxis3 = axes('Parent',dbgFig3);
				dbgFig4 = figure;
				dbgAxis4 = axes('Parent',dbgFig4);
			end
		
			imagesc(img,'Parent',get(dbgFig1,'CurrentAxes'));
			set(dbgFig1,'ColorMap',colormap('gray'));
			title('Original image','Parent',get(dbgFig1,'CurrentAxes'));
			imagesc(congImg,'Parent',get(dbgFig2,'CurrentAxes'));
			set(dbgFig2,'ColorMap',colormap('gray'));
			title('Congealed image','Parent',get(dbgFig2,'CurrentAxes'));
			imagesc(avgImgEye,'Parent',get(dbgFig3,'CurrentAxes'));
			set(dbgFig3,'ColorMap',colormap('gray'));
			title('Average w/o congealing','Parent',get(dbgFig3,'CurrentAxes'));
			imagesc(avgImg,'Parent',get(dbgFig4,'CurrentAxes'));
			set(dbgFig4,'ColorMap',colormap('gray'));
			title('Average with congealing','Parent',get(dbgFig4,'CurrentAxes'));
			drawnow;
			if conf.debugInteractive
				input(' Press enter...');
			end
		end
	end
		%%%%%%%%% DEBUG [1] END   %%%%%%%%%%

end;
%hold all;
%fh = mvpr_lopen(imgFile_, 'read');
%for imgInd = 1:imgTot
%	if conf.useCongealing == true
%		filepair = mvpr_lread(fh);

%		% Load landmarks 
%		[imgDir imgName imgExt] = fileparts(filepair{1});
%		if (isempty(conf.saveExt))
%			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
%			'_' siS_.seedID '_landmarks.mat']; 
%		else
%			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
%			'_' siS_.seedID '_landmarks_' conf.saveExt '.mat']; 
%		end	
%		load(landmarks_SaveFile,'Tseed','T','siS', 'tpointS');

%		% Load ground-truth landmarks
%		[gtimgDir gtimgName gtimgExt] = fileparts(siS_.seedImgFile);
%		
%		gtImageDir = regexprep(gtimgDir, conf.imgDir, conf.gtDir);
%		gtLandmarksFile = [fullfile(gtImageDir, gtimgName) '.dot'];
%		gtSeedPoints = load(gtLandmarksFile);
%		gtSeedPoints = gtSeedPoints';		
%		
%		gtLandmarksFile = [fullfile(gtImageDir, imgName) '.dot'];
%		gtPoints = load(gtLandmarksFile);
%		gtPoints = gtPoints';

%		%% plot dots
%		%keyboard;
%		pp = mvpr_h2d_trans(gtPoints, T(:,:,1));
%		
%		plot(pp(1,1), pp(2,1),'yo')			
%		plot(pp(1,2), pp(2,2),'mo')			
%		plot(pp(1,3), pp(2,3),'co')			
%		plot(pp(1,4), pp(2,4),'ro')			
%		plot(pp(1,5), pp(2,5),'go')			
%		plot(pp(1,6), pp(2,6),'bo')			
%	end
%end
avgImg = avgImg ./ (255*imgTot);
% Return image
imshow(avgImg);
title('Average image');
mvpr_lclose(fh);
hold off;
fh = figh;

