function []=direct_alignment_show_results(imgListFile_, varargin)

% Note that ip_method specific parameters set in the next step
conf = struct('imgDir', '.',...
              'tempDir', 'TEMPWORK',...
	      'gtDir', '.');
conf = mvpr_getargs(conf, varargin);

img_inds = 1:mvpr_lcountentries(imgListFile_);

list_fd = mvpr_lopen(imgListFile_,'read');
for img_ind = img_inds
  % read image
  list_str = mvpr_lread(list_fd);
  img = imread(fullfile(conf.imgDir,list_str{1}));
  % read gt and estimated bbox
  bbox_gt = load(fullfile(conf.gtDir,list_str{2}));
  [bbox_p bbox_f bbox_e] = fileparts(list_str{2});
  bbox_est = load(fullfile(conf.tempDir,bbox_p,[bbox_f '.box_est']));
  clf
  imshow(img);
  hold on;
  % Plot bbox rectangle clock-wise from the upper left corner 
  plot([bbox_gt(1,1) bbox_gt(2,1) bbox_gt(3,1) bbox_gt(4,1) bbox_gt(1,1)],...
       [bbox_gt(1,2) bbox_gt(2,2) bbox_gt(3,2) bbox_gt(4,2) bbox_gt(1,2)],...
       'g-','LineWidth',4);
  %bbox_gt2 = [min(bbox_gt(:,1)) max(bbox_gt(:,1)) min(bbox_gt(:,2)) max(bbox_gt(:,2))];
  %plot([bbox_gt2(3) bbox_gt2(4) bbox_gt2(4) bbox_gt2(3) bbox_gt2(3)],...
  %     [bbox_gt2(1) bbox_gt2(1) bbox_gt2(2) bbox_gt2(2) bbox_gt2(1)],...
  %     'b--','LineWidth',4);

  %plot([bbox_est(3) bbox_est(4) bbox_est(4) bbox_est(3) bbox_est(3)],...
  %     [bbox_est(1) bbox_est(1) bbox_est(2) bbox_est(2) bbox_est(1)],...
  %     'r-','LineWidth',4);
  plot([bbox_est(1,1) bbox_est(2,1) bbox_est(3,1) bbox_est(4,1) bbox_est(1,1)],...
       [bbox_est(1,2) bbox_est(2,2) bbox_est(3,2) bbox_est(4,2) bbox_est(1,2)],...
       'r--','LineWidth',4);
  hold off;
  input('<RETURN>');
end;
mvpr_lclose(list_fd);

