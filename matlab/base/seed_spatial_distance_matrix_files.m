% Calculates feature spatial scores for automatic landmark selection
%
% [spatScoreHistMax, spatScoreHistSum] = ...
%        seed_spatial_distance_matrix_files(imgListFile, siS)
%
% Goes through the images in imgListFile, loads their (seed specific)
% feature distance matrices and then computes spatial matches
% between the features. 
%
% Inputs:
%  imgListFile   - A list of images to be processed, one per line,
%                  containing no white spaces (this cuts the name) and
%                  with their full path (if 'imgDir' not specified).
%  siS           - Seed image structure.
%
% <Optional>
% 'transform'    - Spatial transformation
%                  'isometry'
%                  'similarity' (Def.)
%                  'affinity'
% 'matchRank'    - Corresponds selected within matchRank best
%                  matches according to feature distance from image
%                  points (e.g. 1 if only the best and Inf if all)
%                  (Def. 10).
% 'iters'        - Number of random iterations (Def. 1000).
% 'hitDist'     - Feature minimum distance to be accepted (Def. 0.02).
% 'usePixelDist' - Use pixel distances instead of normalized distance
%                  (Def. false)
% 'tempSaveDir'  - Directory for temporary save items
%                  (e.g. extracted descriptors) (Def: '.')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgFile_) (Def: '/')
% 'enforce'      - Enforce computing even if the file exists
%                  (Def. true)
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
%
% Outputs:
%  spatScoreHistMax - Spatial score histogram for all seed features
%                     over all images (maximum rule).
%  spatScoreHistSum - Spatial score histogram for all seed features
%                     over all images (sum rule).
%
% References:
%
% See also O3D2D_AUTOLM_SELECT_SEED_FILES and 
%          O3D2D_AUTOLM_FEATURE_DISTANCEMATRIX_FILES
%
function [spatScoreHistMax, spatScoreHistSum, spatScoreImg] = ...
	seed_spatial_distance_matrix_files(imgListFile_, siS_, varargin)

%
% Check required functionality
if (exist('mvpr_getargs') == 0)
	error(['MVPR matlab library required for calculation'])
end

%
% Parse input arguments
conf = struct('transform','similarity',...
              'featListFile', [],...
              'tempDir', 'TEMPWORK',...
              'imgDir', '.',...
              'matchRank', 5,...
              'iters', 1000,...
              'hitDist', 0.05,...
              'saveExt', '',...
              'debugLevel', 0,...
              'usePixelDist', false,...
              'enforce', true,...
              'spatMatchingMethod', 'RANSAC');
conf = mvpr_getargs(conf,varargin);

% The default is the same as in MVPR_FEATURE_EXTRACT_FILES
if isempty(conf.featListFile)
    [fpath fname fext] = fileparts(imgListFile_);
    featListFile = fullfile(conf.tempDir,[fname '_feat' fext]);
else
    featListFile = conf.featListFile;
end;

if (~exist(featListFile))
    error(['List file ' featListFile 'containing images and extracted local'...
           'features not found - run mvp_feature_extract_files() for ' ...
           'your images first!']);
end;

% Count number of images
imgTot = mvpr_lcountentries(featListFile);

% Initialize
spatScoreHistMax = zeros(size(siS_.descriptors,2),1);
spatScoreHistSum = 0;

% timing
timeTotalEstimated = 0;
timeTotalElapsed = 0;
timeElapsed = 0;

% errors
failed = 0;
if conf.debugLevel > 0
	fprintf('Computing spatial scores\n');
end
fh = mvpr_lopen(featListFile, 'read');

if (conf.debugLevel > 1)
	figh = figure();
end
% 
% Main loop:
% Go through images and form the distance matrices
for imgInd = 1:imgTot
  thisFailed = 0;
  timeTotalEstimated = (timeTotalElapsed / (imgInd-1)) * (imgTot-imgInd);
	if conf.debugLevel > 0
		fprintf('\r  * Progress: %5d/%5d, ETA: %.1fs (== %.1f mins or %.1f hours)  ',...
                        imgInd, imgTot, timeTotalEstimated,timeTotalEstimated/60,timeTotalEstimated/3600);
	end
	tic;
	filepair = mvpr_lread(fh);
        
	% Test if spatscorematr is already computed with this seed
	[imgDir imgName imgExt] = fileparts(filepair{2});

	if (isempty(conf.saveExt))
		spatscorematr_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
		'_' siS_.seedID '_spatscorematr.mat']; 
	else
		spatscorematr_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
		'_' siS_.seedID '_spatscorematr_' conf.saveExt '.mat']; 
	end			        
	if (exist(spatscorematr_SaveFile,'file') && (conf.enforce == false))
		load(spatscorematr_SaveFile, 'fullScoreMatr', 'scoreMatrMask');
	else % Make image specific computation
		% Load descriptors
                load(fullfile(conf.tempDir,filepair{3}),'descriptors','frames');
                
		% Load feature distance matrices
		fdistmatr_SaveFile = [fullfile(conf.tempDir, imgDir, imgName)...
				      '_' siS_.seedID '_fdistmatr.mat']; 
		load(fdistmatr_SaveFile,'fdistmatr','siS');
				
		% ignore if this is seed
		isSeed = false;
                [foo imgname foo] = fileparts(filepair{3});
                if strmatch(imgname,siS_.seedID)
                  fprintf('Seed: ignored\n');
                end

		if ~isSeed
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
			%if (conf.debugLevel >= 0)
			%	if (exist('seedImg') == false)
			%		seedImg = imread(siS_.seedImgFile);
			%	end
			%	currImg = imread(fullfile(conf.imgDir,filepair{1}));
			%else
			%	seedImg = [];
			%	currImg = [];
			%end
			%%%%%%%%% DEBUG [1] END   %%%%%%%%%%
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			try
                          [fullScoreMatr,scoreMatrMask] = ...
                              seed_spatial_distance_matrix(siS_,...
                                                           frames(1:2,:),  ...
                                                           fdistmatr,      ...
                                                           'transform',    conf.transform,...
                                                           'matchRank',    conf.matchRank,...
                                                           'iters',        conf.iters,...
                                                           'hitDist',      conf.hitDist,...
                                                           'debugLevel',   conf.debugLevel,...
                                                           'usePixelDist', conf.usePixelDist,...
                                                           'method',    conf.spatMatchingMethod);
				
			catch exception
                          warning(['Spatial Score calculation failed: '...
                                   exception.message]);
				fullScoreMatr = zeros([size(fdistmatr,1)]);
				scoreMatrMask = zeros([size(fdistmatr,1)]);
                                thisFailed = true;
				failed = failed + 1;
			end
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
			if (conf.debugLevel > 0 && ~thisFailed)
				%keyboard;
				if (exist('seedImg') == false)
					seedImg = imread(fullfile(conf.imgDir,siS_.seedImgFile));
					seedFrames = siS_.frames;
				end
				currImg = imread(fullfile(conf.imgDir,filepair{2}));

				imgs{1} = seedImg;
				imgs{2} = currImg;

				iframes{1} = seedFrames;
				iframes{2} = frames;

				[v idx] = max((fullScoreMatr.*scoreMatrMask)');
				[v2 idx2] = sort(v,'descend');
				
				% Take only unique matches (look better with fewer lines)
				[xx xxi] = unique(idx(idx2),'stable');
                                matches2 = xx(1:20);
                                matches1 = xxi(1:20)';
                                % THIS IS HOW IT USED TO BE UNTIL
                                % FEB 4 2015 - DONT UNDERSTAND HOW
                                % IT EVEN WORKED?!
                                %matches1 = idx2(xx(1:20));
                                %matches2 = idx(matches1);
				matches = [matches1; matches2];

                                if (conf.debugLevel > 1)
                                  figh = plot_matches(imgs,iframes,matches,'handle',figh);
                                  title(sprintf('unique matches (1-%d)', xx(20)));
                                  drawnow;
                                end;
                                if conf.debugLevel > 2
                                  input('<RETURN>');
                                end;
				%keyboard;
				%o3d2d_autolm_dbg_plotnbestmatches(seedImg,siS_.siftFrames,...
				%                                  currImg,siftFrames,...
				%                                  fullScoreMatr.*scoreMatrMask,...
				%                                  10,...
				%                                  'scoreOrient','max');
				%[foo sortInd] = sort(sort(fullScoreMatr.*scoreMatrMask,2,'descend'),'descend');
				%foomatr = inf(size(fdistmatr));
				%foomatr(sortInd(1:10),:) = 1;
				%[v1 idx1] = max(fullScoreMatr.*scoreMatrMask, [], 2);
				%[v2 idx2] = sort(v1,'descend');
				%keyboard;
				%[i j] = find(fullScoreMatr.*scoreMatrMask > 12);
				%[v2 idx2] = sort(fullScoreMatr,'descend');
				%x = 1+rem(idx2(1:10)-1,size(spatScoreHistMax,1))
				%y = 1+floor((idx2(1:10)-1)/size(spatScoreHistMax,1))
				%debugPlot(seedImg, siS.frames(:,idx2(1:10)), currImg, frames(:,1:10));
				%drawnow;
%				o3d2d_autolm_dbg_plotnbestmatches(seedImg,siS_.siftFrames,...
%							          currImg,siftFrames,...
%							          foomatr.*fdistmatr,...
%							          10,...
%							          'scoreOrient','min');
				%pause;
			end
			%%%%%%%%% DEBUG [1] END   %%%%%%%%%%
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%keyboard;
			% Save all data
			siS = siS_; % dummy repeat
			
			save(spatscorematr_SaveFile, 'fullScoreMatr', 'scoreMatrMask');
		else
			%warning('seed ignore');
			fullScoreMatr = zeros([size(fdistmatr)]);
			scoreMatrMask = zeros([size(fdistmatr)]);
			siS = siS_; % dummy repeat
			save(spatscorematr_SaveFile, 'fullScoreMatr', 'scoreMatrMask');
		end
		
	end
	
	% Make global computation
	spatScoreHistMax = spatScoreHistMax + max(fullScoreMatr.*scoreMatrMask, [], 2);
	spatScoreHistSum = spatScoreHistSum + sum(fullScoreMatr.*scoreMatrMask, 2);
	spatScoreImg(imgInd) = sum(sum(fullScoreMatr.*scoreMatrMask));
	
	timeElapsed = toc;
	timeTotalElapsed = timeTotalElapsed + timeElapsed;
end
mvpr_lclose(fh);

%
% Load/Save spatial scores
%spatscoreStatFile = [fullfile(conf.tempDir, siS_.seedID) ...
%                     conf.saveExt ...
%                     '_spatscore_stats.mat']; 
%if (exist(spatscoreStatFile,'file') && (conf.enforce == false))
%	load(spatscoreStatFile,'spatScoreHistSum','spatScoreHistMax','siS');
%else
%	%fprintf('* Saving statistics to ''%s''!\n', spatscoreStatFile);
%	siS = siS_; % dummy repeat
%	save(spatscoreStatFile,'spatScoreHistSum','spatScoreHistMax','siS');
%end

%
% Save seed landmark statistics (max & sum)
%seedlmFile = [fullfile(conf.tempDir,siS_.seedID) ...
%              conf.saveExt ...
%              '_max_landmarks.txt']; 
%if (exist(seedlmFile,'file') && (conf.enforce == false))
%	spatScoreHistMax = load(seedlmFile);
%else
%	%fprintf('* Saving seed max landmarks to ''%s''!\n',seedlmFile);
%	lmFh = mvpr_lopen(seedlmFile,'write');
%	[bestScore bestInd] = sort(spatScoreHistMax,'descend');
%	for lmInd = 1:length(bestInd)
%		mvpr_lwrite(lmFh,sprintf('%f %f %f',...
%			    siS_.frames(1,bestInd(lmInd)),... % x
%			    siS_.frames(2,bestInd(lmInd)),... % y
%			    bestScore(lmInd))); % score
%	end
%	mvpr_lclose(lmFh);
%end

%seedlmFile = [fullfile(conf.tempDir,siS_.seedID) ...
%              conf.saveExt ...
%              '_sum_landmarks.txt'];
%if (exist(seedlmFile,'file') && (conf.enforce == false))
%	spatScoreHistMax = load(seedlmFile);
%else
%	%fprintf('* Saving seed sum landmarks to ''%s''!\n',seedlmFile);
%	lmFh = mvpr_lopen(seedlmFile,'write');
%	[bestScore bestInd] = sort(spatScoreHistSum,'descend');
%	for lmInd = 1:length(bestInd)
%		mvpr_lwrite(lmFh,sprintf('%f %f %f',...
%			    siS_.frames(1,bestInd(lmInd)),... % x
%			    siS_.frames(2,bestInd(lmInd)),... % y
%			    bestScore(lmInd))); % score
%	end
%	mvpr_lclose(lmFh);
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
if (conf.debugLevel > 1)
	if (exist('seedImg') == false)
		seedImg = imread(fullfile(conf.imgDir,siS_.seedImgFile));
	end
	%subplot(1,2,1);

	%o3d2d_autolm_dbg_plotnbestlandmarks(...
	%seedImg,siS_.siftFrames,spatScoreHistMax,10,10,'scoreOrient','max');
	%title('max rule');
	%subplot(1,2,2);
	%o3d2d_autolm_dbg_plotnbestlandmarks(...
	%    seedImg,siS_.siftFrames,spatScoreHistSum,10,10,'scoreOrient','max');
	%title('sum rule');
end
%%%%%%%%% DEBUG [1] END   %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if conf.debugLevel > 0
	fprintf('\r  * Done! Total time elapsed: %.2fs, %.2fs per file.  \n', ...
		timeTotalElapsed, timeTotalElapsed / imgTot);
end
end % end of function


function debugPlot(seedImg, seedFrames, otherImg, otherFrames)
	% Create debug image
	width = size(seedImg,2) + size(otherImg,2);
	height = max([size(seedImg,1) size(otherImg,1)]);	
	dimensions = max([size(seedImg,3) size(otherImg,3)]);
	
	dbgImg = zeros(height, width, dimensions);
	dbgImg(1:size(seedImg,1), 1:size(seedImg,2),:) = seedImg;
	dbgImg(1:size(otherImg,1), 1+size(seedImg,2):width,:) = otherImg;
	
	% Create the view
	imshow(dbgImg/255);
	hold on;
	title('spatial matching for N best matches');
	set(gcf,'DoubleBuffer','on');

	%[v idx] = sort(fdistmatr(:));
	dbgOff = size(seedImg,2);
	for n = 1:size(seedFrames,2)
		
		seedFrame = seedFrames(:,n);
		currFrame = otherFrames(:,n);
	
		plot(seedFrame(1),seedFrame(2),'yd','MarkerSize',10,'LineWidth',2);
		plot(currFrame(1)+dbgOff,currFrame(2),'yd','MarkerSize',10,'LineWidth',2);
		plot([seedFrame(1) currFrame(1)+dbgOff],...
			[seedFrame(2) currFrame(2)]','y-','LineWidth',2);
		text(seedFrame(1),seedFrame(2),num2str(n),...
			'FontSize',12,'color','yellow','backgroundcolor','black');
		text(currFrame(1)+dbgOff,currFrame(2),num2str(n),...
			'FontSize',12,'color','yellow','backgroundcolor','black');
	end
	hold off;
	drawnow;
end

