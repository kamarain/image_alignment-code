%Graph visualisaion

%Author: Fatemeh Shokrollahi Yancheshmeh


function [ ] = Graph_of_bestmatche(GraphVisualize,nbNeighbour,G_cut,edge_info,imgListFile,imgDir,nodeTot)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

 sz = nodeTot;
 Gsprs_cut = sparse(G_cut);
 [row clm  W] = find(Gsprs_cut);
 Gsprs_cut = sparse(row,clm,W,sz,sz);
 bg2 = biograph(Gsprs_cut,[],'ShowWeights','on');
 linecolor =[ 0.1, 0.1, 0.1];
for sourceId = 1: sz
    
   for counter = 1 : nbNeighbour

     sinkId = edge_info(sourceId, counter);
     edgehandle = getedgesbynodeid(bg2, bg2.nodes(sourceId).ID,bg2.nodes(sinkId).ID);
     set(edgehandle,'LineWidth',0.5+nbNeighbour-counter,'LineColor',linecolor);
   end
   
    linecolor =[randi(90)/100,randi(100)/100,randi(100)/100];

end
  
dolayout(bg2);
if GraphVisualize
   embed_images_inNodes( bg2,sz,imgListFile,imgDir,'Graph',nan);
end


end

