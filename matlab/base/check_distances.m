% Checks if coordinates are within distance threshold
%
% [ret] = o3d2d_autolm_checkdistance(coordinates, distanceThreshold)
%
% Input:
%  coordinates       - 2xN coordinates to calculate distance
%  distanceThreshold - The distance threshold
%
% Optional:
%  'distanceNorm'     - Distance normalization value (default: 0)
%  'usePixelDistance' - Use pixel-based distance evaluation (default: true)
%
% Output:
%  ret		     - If the distance between the provided points are
%                      within distance threshold it will return FALSE, otherwise
%                      TRUE
%
% See also:
%


function [ret] = o3d2d_autolm_checkdistance(coordinates, distanceThreshold, varargin)
% Parse input arguments
conf = struct('distanceNorm', 0,...
              'usePixelDistance', true);
conf = mvpr_getargs(conf, varargin);


% by default all points are outside the distance threshold
ret = true;


for fooind = 1:size(coordinates,2)
	fooCoords = coordinates;
	fooCoords(:,fooind) = realmax;
	fooDists = fooCoords-repmat(coordinates(:,fooind),[1 size(fooCoords,2)]);
	if conf.usePixelDistance
		fooDists = sqrt(sum(fooDists.^2,1));
	else
		fooDists = sqrt(sum(fooDists.^2,1)) ./ conf.distanceNorm;
	end
	if (sum(fooDists < distanceThreshold) > 0)
		ret = false;
	end
end

end

%if size(coordinates, 1) > 2
%	error('Too many points for distance checking.');
%else
%	distance = norm(coordinates(1,:) - coordinates(2,:));
%	
%	if conf.usePixelDistance == false 
%		distance = distance / conf.distanceNorm;
%	else
%	
%	if distance < distanceThreshold
%		ret = true;
%	end
%end
