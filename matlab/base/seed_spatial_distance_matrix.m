% Compute scores based on spatial transform scores
%
% [fullScoreMatr,scoreMatrMask] = 
%       o3d2d_autolm_fspatscores(seedPoints, imgPoints, fDistMatr)
%
% Randomly selects min number of seed features in seedPoints, then
% selects their correspondence from image features imgPoints.
% Transforms all seed points according to the estimated
% transformation and accumulates scores for those points that match
% to their correspondence in imgPoints.
%
% Inputs:
%  seedPoints      - 2xN feature locations in the seed image.
%  imgPoints       - 2xM feature locations in the image.
%  fDistMatr       - NxM feature distance matrix between points.
%
% <Optional>
% 'transform'    - Spatial transformation
%                  'isometry'
%                  'similarity' (Def.)
%                  'affinity'
% 'matchRank'    - Corresponds selected within matchRank best
%                  matches according to feature distance from image
%                  points (e.g. 1 if only the best and Inf if all)
%                  (Def. 10).
% 'iters'        - Number of random iterations (Def. 1000).
% 'hitDist'     - Feature minimum distance to be accepted (Def. 0.02).
% 'tempSaveDir'  - Directory for temporary save items
%                  (e.g. extracted descriptors) (Def: '.')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgFile_) (Def: '/')
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
%
% Outputs:
%  -
%
% References:
%  -
% 
function [fullScoreMatr,scoreMatrMask] = ...
    seed_spatial_distance_matrix(siS_, imgP_, fDistMatr_, varargin)

%
% Parse input arguments
conf = struct('transform','similarity',...
              'matchRank',10,...
              'iters',1000,...
              'hitDist', 0.02,...
              'imgDir', 'images/',...
              'debugLevel', 0,...
              'method', 'RANSAC',...
              'usePixelDist', false);
conf = mvpr_getargs(conf,varargin);
seedP_ = siS_.frames(1:2,:);

if (size(fDistMatr_,1) ~= size(seedP_,2) ||...
    size(fDistMatr_,2) ~= size(imgP_,2))
	error(['Size of the feature match matrix and number of image or ' ...
	       'seed features do not match.']);
end

switch conf.transform
	case 'isometry',
		minFeatNum = 2;
	case 'similarity',
		minFeatNum = 2;
	case 'affinity',
		minFeatNum = 3;
	otherwise,
		error('Unknown ''transform''');
end

%
%% Initialize variables
fullScoreMatr = zeros(size(fDistMatr_));
scoreMatr     = zeros(size(fDistMatr_));
[foo rank_fDistMatr] = sort(fDistMatr_, 2, 'ascend');
bestScore     = -inf;
matchRank     = min([conf.matchRank size(fDistMatr_, 2)]); % inf for all

if matchRank < minFeatNum	
	error('too few image matches');
end
if conf.usePixelDist == true
	hitDist = conf.hitDist^2;
else
	hitDist = (conf.hitDist*siS_.norm)^2;
end

if strcmp(conf.method,'RANSAC')
  for iInd = 1:conf.iters
    
    % select random seed features
    foo = randperm(size(fDistMatr_, 1));
    seedPointInds = foo(1:minFeatNum);
    seedPointCoords = seedP_(:, seedPointInds);
    
    % Test that seed points are separate
    if (~check_distances(seedPointCoords, conf.hitDist, ...
                        'usePixelDistance', conf.usePixelDist,...
                        'distanceNorm', siS_.norm) )
      continue;
    end
    
    % select random corresponding image features
    imgPointCandidates = rank_fDistMatr(seedPointInds, 1:matchRank);
    sMatches = [];
    for fooind = 1:minFeatNum
      sMatches = [sMatches randperm(matchRank)'];
    end
    
    imgPointInds = diag(imgPointCandidates(:, sMatches(1,:)'));
    imgPointCoords = imgP_(:, imgPointInds);
    
    % I am not sure if testing this makes any sense /Joni
    % Test that are separate at least by two pixels (otherwise singular)
    if (~check_distances(imgPointCoords, 2) )
      continue;
    end
    
    % estimate transformation
    T = mvpr_h2d_corresp(imgPointCoords, seedPointCoords,...
                         'hType', conf.transform);
    
    % transform all image points
    TseedPointCoords = mvpr_h2d_trans(imgP_, T);
    
    % check which points match (distance smaller than threshold)
    %	distMatrX = repmat(seedP_(1,:),            [size(imgP_,2) 1])-...
    %	            repmat(TseedPointCoords(1,:)', [1 size(seedP_,2)]);
    %	
    %	distMatrY = repmat(seedP_(2,:), [size(imgP_,2) 1])-...
    %	            repmat(TseedPointCoords(2,:)', [1 size(seedP_,2)]);
    %
    %	% Do normalization if needed
    %	if conf.usePixelDist == true
    %		distMatr = sqrt(distMatrX.^2 + distMatrY.^2);
    %	else
    %		distMatr = sqrt(distMatrX.^2 + distMatrY.^2) ./ siS_.norm;		
    %	end
    
    [distMatr] = mvpr_feature_matchmatrix(TseedPointCoords,seedP_);
    distMatr = distMatr'; % because changed projection direction
                          % Give the features within the minimum distance a score point
    thdistMatr = zeros(size(distMatr));
    thdistMatr(find(distMatr < hitDist)) = 1;
    thdistMatr(seedPointInds,:) = 0; % eliminate seeds
    
    % Increase total score
    fullScoreMatr = fullScoreMatr + thdistMatr;
  end;
end
if strcmp(conf.method,'fminsearch')
  switch conf.transform
   case 'isometry',
    initT = [0 0 0];
   case 'similarity',
    initT = [0 0 0 1];
   otherwise,
    error('Unknown ''transform'' for fminsearch spatial score!');
  end

  fminopts = optimset('fminsearch');
  %fminopts = optimset(fminopts, ...
  %                    'MaxFunEvals',conf.iters,...
  %                    'TolX',0.1);
  %                    'TolFun',1);
  optT  = fminsearch(@match_perf2,initT,fminopts);
  
  % Total score is the best transformation
  fullScoreMatr = thdistMatr;
end;

[sortFullScore] = sort(fullScoreMatr, 2, 'descend');
scoreMatrMask = fullScoreMatr - ...
	        repmat(sortFullScore(:, matchRank), [1 size(fullScoreMatr, 2)]);
scoreMatrMask = scoreMatrMask >= 0;
return
%
% Nested function for optimisation
%  * All varibales here visible to the main function as well
function mp = match_perf2(trans_)

% scale to some meaningful scale
%strans = [100000*trans_(1) 100000*trans_(2) 100000*trans_(3) trans_(4)]
strans = [25000*trans_(1) 25000*trans_(2) 5000*trans_(3) trans_(4)];
%input('R');
switch conf.transform
 case 'isometry'
  T = mvpr_h2d_iso(strans(3),[strans(1) strans(2)]);
 case 'similarity'
  T = mvpr_h2d_sim(strans(3),[strans(1) strans(2)],strans(4));
 otherwise
  error('Unknown transformation!');
end;

% transform all image points
TimgP = mvpr_h2d_trans(imgP_, T);

[coord_distMatr] = mvpr_feature_matchmatrix(TimgP,seedP_);
coord_distMatr = coord_distMatr'; % because changed projection direction
% Mask with the best matches
rank_fDistMatr_mask = inf(size(coord_distMatr));
rank_fDistMatr_mask(rank_fDistMatr <= conf.matchRank) = 1;
coord_distMatr = coord_distMatr.*rank_fDistMatr_mask;

% Give the features within the minimum distance a score point
thdistMatr = zeros(size(coord_distMatr));
thdistMatr(find(coord_distMatr < hitDist)) = 1;
mp = -sum(thdistMatr(:));
end

end % main function

