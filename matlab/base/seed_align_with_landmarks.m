% Matches spatially landmarks with images
%
% [T scores] = o3d2d_autolm_match_landmarks(imgListFile, siS, seedLandmarks)
%
% Goes through the images in imgFile_, loads their (seed specific)
% feature distance matrices and then computes spatial matches
% between the features. Finally the full accumulator (accS) is
% stored.
%
% Inputs:
%  imgFile_ - A list of images to be processed, one per line,
%             containing no white spaces (this cuts the name) and
%             with their full path (if 'imgDir' not specified).
%  siS      - Seed image structure.
%  n_       - Number of best landmarks used.
%
% <Optional>
% 'useSpatial'	 - Use spatial score matrix to select corresponding points
% 'transform'    - Spatial transformation
%                  'isometry'
%                  'similarity' (Def.)
%                  'affinity'
% 'matchRank'    - Corresponds selected within matchRank best
%                  matches according to feature distance from image
%                  points (e.g. 1 if only the best and Inf if all)
%                  (Def. 5).
% 'iters'        - Number of random iterations (Def. 1000).
% 'hitDist'     - Feature minimum distance to be accepted (Def. 5 (pixels)).
% 'tempDir'  - Directory for temporary save items
%                  (e.g. extracted descriptors) (Def: '.')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgFile_) (Def: '/')
% 'saveExt'      - Extension to be added landmark files: 
%                  <tempDir><SeedID>landmarks<saveExt>.mat
% 'usePixelDist' - Use pixel-based distance instead of normalized distance 
%                  (default: false)
% 'enforce'      - Enforce computing even if the file exists (Def. true)
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
% 'retMatches'   - How many best matches are returned. (Def. 1)
%
% Outputs:
%  
%
% References:
%
% See also:
%
function [T scores fscores] = o3d2d_autolm_match_landmarks_files(imgListFile_, siS_,...
                                                 seedLmInds_,varargin)

%
% Parse input arguments
conf = struct('useSpatial', false,...
              'featListFile', [],...
              'transform', 'similarity',...
              'matchRank', 5,...
              'iters', 1000,...
              'hitDist', 0.02,...
              'tempDir', 'temp/',...
              'imgDir', 'images/',...
              'saveExt', [],...
              'debugLevel', 0, ...
              'usePixelDist', false,...
              'enforce',    true,...
              'retMatches', 1);
conf = mvpr_getargs(conf,varargin);

% The default is the same as in MVPR_FEATURE_EXTRACT_FILES
if isempty(conf.featListFile)
    [fpath fname fext] = fileparts(imgListFile_);
    featListFile = fullfile(conf.tempDir,[fname '_feat' fext]);
else
    featListFile = conf.featListFile;
end;

if (~exist(featListFile))
    error(['List file ' featListFile 'containing images and extracted local'...
           'features not found - run mvp_feature_extract_files() for ' ...
           'your images first!']);
end;


% Count number of images
imgTot = mvpr_lcountentries(featListFile);
fh = mvpr_lopen(featListFile, 'read');

% timing
timeTotalEstimated = 0;
timeTotalElapsed = 0;
timeElapsed = 0;

% init
scores = [];
T = [];
Ts = {};
if conf.debugLevel > 0
	fprintf('Matching landmarks\n');
end

%
% Main loop:
% Go through images and find the best transformation
for imgInd = 1:imgTot
    %for imgInd = setxor(1:imgTot,siS_.seedImgNo), % We dont need to process seed image
	timeTotalEstimated = (timeTotalElapsed / (imgInd-1)) * (imgTot-imgInd);
	if conf.debugLevel > 0
		fprintf('\r  * Progress: %5d/%5d, ETA: %.1fs  ', imgInd, imgTot, timeTotalEstimated);
	end
	tic;
	
	filepair = mvpr_lread(fh);
	% Test if landmarks already computed with this seed
	[imgDir imgName imgExt] = fileparts(filepair{2});
	
	if (isempty(conf.saveExt))
		landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
		'_' siS_.seedID '_landmarks.mat']; 
	else
		landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
		'_' siS_.seedID '_landmarks_' conf.saveExt '.mat']; 
	end
	if exist(landmarks_SaveFile, 'file') && conf.enforce == false
		load(landmarks_SaveFile, 'Tseed', 'T', 'siS', 'scores','fscores');
	else
		% Load descriptors
                load(fullfile(conf.tempDir,filepair{3}),'descriptors','frames');
                
		% Load feature distance matrix
		fdistmatr_SaveFile = [fullfile(conf.tempDir, imgDir, imgName)...
			              '_' siS_.seedID '_fdistmatr.mat'];
		load(fdistmatr_SaveFile,'fdistmatr');
		[sortFdist] = sort(fdistmatr,2,'ascend');
		
		if size(fdistmatr,2) < conf.matchRank
			%if size(fdistmatr,2) == 0
			%
			%else
			%	conf.matchRank = size(fdistmatr,2);			
			%end
					
			Tt = eye(3);
			for i = 1:conf.retMatches
				T(:,:,i) = Tt;
			end
			scores(i,:) = inf(1,conf.retMatches);	
			tpointS = {};
			warning('too few image features');
		else
		
			fdistmatrMask = fdistmatr - ...
					repmat(sortFdist(:,conf.matchRank), [1 size(fdistmatr,2)]);
			fdistmatrMask = fdistmatrMask <= 0;

			if (conf.useSpatial) % Use spatial score matrix
				% Load spatial score matrix
				fprintf('Using Spatial!');

				if (isempty(conf.saveExt))
					spatscorematr_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
					'_' siS_.seedID '_spatscorematr.mat']; 
				else
					spatscorematr_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
					'_' siS_.seedID '_spatscorematr_' conf.saveExt '.mat']; 
				end
				
				load(spatscorematr_SaveFile,'fullScoreMatr');
				[sortFullScore] = sort(fullScoreMatr,2,'descend');
				scoreMatrMask = fullScoreMatr-...
						repmat(sortFullScore(:,conf.matchRank), ...
						[1 size(fullScoreMatr,2)]);
				scoreMatrMask = scoreMatrMask >= 0;

				% Select only the best from corresponding image points
				bestMatch = fullScoreMatr.*scoreMatrMask;
				spoints = siS_.frames(1:2, seedLmInds_);
				for lmInd = 1:length(seedLmInds_)
					candInds = find(squeeze(scoreMatrMask(seedLmInds_(lmInd), :)) > 0);
					tpointS(lmInd,:) = frames(1:2, candInds);
				end
			else % use distances (default)
				% Select only the best from corresponding image points
				bestMatch = fdistmatr.*fdistmatrMask;
				spoints = siS_.frames(1:2,seedLmInds_);
				for lmInd = 1:length(seedLmInds_)
					%seedLmInds_
					%size(fdistmatr)
					candInds{lmInd} = find(squeeze(fdistmatrMask(seedLmInds_(lmInd), :)) > 0);
					%tpointS(1:2,lmInd) = siftFrames(1:2,candInds(1));
				
					tpointS{lmInd} = frames(1:2,candInds{lmInd});
				end
			
			end

			[T scores idx inliers] = mvpr_h2d_corresp_multi(...
			             tpointS, ...
			             spoints, ...
			             conf.hitDist * siS_.norm, ...
				     'hType', conf.transform, ...
				     'riters', conf.iters, ...
				     'numBest', conf.retMatches);
			
			fsc = inf(length(inliers),length(seedLmInds_));
			for k=1:length( inliers ),				
				for fooInd = 1:size(inliers{k},2)
					if (~isnan(inliers{k}(fooInd)))
						x = candInds{fooInd}(inliers{k}(fooInd));
						fsc(k,fooInd) = fdistmatr(seedLmInds_(fooInd),x);
					end
				end
				fscores{imgInd} = fsc;
			end;
%			fsc = fsc / size(inliers,2);
			
%												
%			candInds1 = cell2mat(candInds');
%			ind1 = candInds1(~isnan(inliers),:)';
%			ind = ind1(inliers(find(inliers > 0)) + [0:5:5*(size(idx{1},2)-1)]);
%			
			%keyboard;

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
			if (conf.debugLevel > 1)
	%			keyboard;
	%			o3d2d_autolm_plot_matches(imread(siS_.seedImgFile), siS_.frames, ...
	%			                          imread(fullfile(conf.imgDir,filepair{1})), frames, ...
	%			                          [idx{1}; seedLmInds_]);

	%		        o3d2d_autolm_dbg_plotnbestmatches(...
	%			imread(siS_.seedImgFile),...
	%			siS_.siftFrames(:,seedLmInds_),...
	%			imread(fullfile(conf.imgDir,filepair{1})),...
	%			Tseed,...
	%			eye(size(Tseed,2)),length(seedLmInds_),'scoreOrient','max');
				%hold on;	
				%keyboard;
			end
			%%%%%%%%% DEBUG [1] END   %%%%%%%%%%
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		end
		
		if(size(T, 2) >= 1)
			Tseed = mvpr_h2d_trans(siS_.frames(1:2, seedLmInds_), T(:,:,1));
		else
			Tseed = [];
			error('FIX IT')
		end		
		
		% Save all data
		siS = siS_; % dummy repeat
		if exist(fullfile(conf.tempDir,imgDir)) == 7
			
			save(landmarks_SaveFile, 'Tseed', 'T', 'siS', 'tpointS', 'scores','fscores');
		else
			mkdir(fullfile(conf.tempDir,imgDir))
			save(landmarks_SaveFile, 'Tseed', 'T', 'siS', 'tpointS', 'scores','fscores');
		end
		

	end
	scoress{imgInd} = scores;

	Ts{imgInd} = T;
	timeElapsed = toc;
	timeTotalElapsed = timeTotalElapsed + timeElapsed;
end

mvpr_lclose(fh);

if conf.debugLevel > 0
	fprintf('\r  * Done! Total time elapsed: %.2fs, %.2fs per file.  \n', ...
	        timeTotalElapsed, timeTotalElapsed / imgTot);
end

T = Ts;
scores = scoress;
end % end of function
