%
%INPUTS:
%       pred: is a vector in which the columns show the nodes number and the
%       elements stored at each column is the parent of the node. e.g:
%       pred = [3 4 2 0 3]. pred(1) = 3 => node number 3 is the
%       parent of the node number 1. zero represents root.
% 
%       childNo: is the nodeNumber from which we are going to find the path
%       to the root.
%OUTPUTS:
%        path: vector including visited nodes along the path from child to
%        the root.
%        transLink: cell array includes vectro representing source and sink
%        nodes at each link along the path from child to root. 
%        e.g  { [sinkid source] [sink source]....}

% Author: Fateme shokrollahi Yancheshmeh
%%
function [transLink path]  = MST_shortpatth(pred,chiledNo )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n = 0;
path = [];
transLink = [];
parent = pred(chiledNo);
while parent ~= 0
    
    n = n + 1;
    path(n) = chiledNo;
    parent = pred(chiledNo);
      if  parent ~= 0
          transLink{n} =[parent  chiledNo];
      end
    chiledNo = parent;
     
end

end

