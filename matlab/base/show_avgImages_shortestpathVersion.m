
%INPUTS: 
%       newT : transformation matrix along the shortes path
%       imgFile_ : text file including imagenames and their directory at
%       each line


%
%AUG 2013 Fatemeh

function [fh avgImg] = show_avgImages_shortestpathVersion(imgFile_,siS_,newT, varargin )

%
% Parse input arguments
conf = struct('avgImgSize', [],...
              'crop', [],...
              'saveExt', [],...
              'tempDir', 'temp/',...
              'imgDir', 'images/',...
              'gtDir', '/',...
              'debugLevel', 0,...
              'debugInteractive', true,...
              'isVisible', true,...
              'noAlignment', false, ...
              'useIdeal', false,...
              'useBest', false);
conf = mvpr_getargs(conf,varargin);

% Count number of images
imgTot = mvpr_lcountentries(imgFile_);
if isempty(conf.avgImgSize )
  % count the mean size of images
  szI = zeros(imgTot,1);
  szJ = zeros(imgTot,1);
  fh = mvpr_lopen(imgFile_, 'read');
  for imgInd = 1:imgTot
    filepair = mvpr_lread(fh);
   
     if (length(filepair) == 1) % assuming just a list of images
      img = imread(fullfile(conf.imgDir,filepair{1}));
    else % assuming a feature list produced by our methods
      img = imread(fullfile(conf.imgDir,filepair{2}));
    end;
    szI(imgInd) = size(img,1);
    szJ(imgInd) = size(img,2);
  end;
  mvpr_lclose(fh);
  avgImgSize = [round(median(szJ,1)*1.2) round(median(szI,1)*1.2)];
else
  avgImgSize = conf.avgImgSize;
end;
% Main loop:
%  Go through images and find the best transformation
fh = mvpr_lopen(imgFile_, 'read');

% Creating the matrix to be plotted
avgImg = zeros(avgImgSize(2), avgImgSize(1),3);
avgImgEye = zeros(avgImgSize(2), avgImgSize(1),3);
avgOrigo = round([avgImgSize(2)/2 avgImgSize(1)/2]);

% Main loop:
%  Go through images and find the best transformation
fh = mvpr_lopen(imgFile_, 'read');

if ~conf.isVisible
	figh = figure('visible','off');
else
	figh = figure; 
end
hold all;

% %**************************************************************************
% 
%  fa =  avgImg ;
% te =avgImgEye ; 
% me=avgOrigo  ;
% %**************************************************************************
fprintf('Creating an average plot\n');
for imgInd = 1:imgTot
%      %*********************************************************************
%     avgImg = fa;
%    avgImgEye = te;
%    avgOrigo = me;
%     %**********************************************************************
    
    if conf.noAlignment == true
		filepair = mvpr_lread(fh);
		 if (length(filepair) == 1) % assuming just a list of images
                  img = imread(fullfile(conf.imgDir,filepair{1}));
                else % assuming a feature list produced by our methods
                  img = imread(fullfile(conf.imgDir,filepair{2}));
                end;

                if ndims(img) < 3 % enforce RGB
                  img = repmat(img, [1 1 3]);
                end;
                  
                
		d = floor(([size(avgImg,1) size(avgImg,2)]-...
                           [size(img,1) size(img,2)])/2);
	
		posX = 1;
		posY = 1;
	
		startX = 1;
		startY = 1;
	
		if d(1) > 0
			posY = d(1);
		else
			startY = -d(1);
		end
		if d(2) > 0
			posX = d(2);
		else
			startX = -d(2);
		end
		if posX == 0
			posX = 1;
		end
		if posY == 0
			posY = 1;
		end
		if startX == 0
			startX = 1;
		end		
		if startY == 0
			startY = 1;
        end			
	    clippedImg = img(startY:size(img,1)-startY-1, startX:size(img,2)-startX-1,:);
		clippedImg = double(clippedImg);	
		
        avgImg(posY:size(clippedImg,1)+posY-1, posX:size(clippedImg,2)+posX-1,:) = ...
			   avgImg(posY:size(clippedImg,1)+posY-1, posX:size(clippedImg,2)+posX-1,:) + ...
			   clippedImg;	
    else
        
        filepair = mvpr_lread(fh);
        
        % Load landmarks 
        
		[imgDir imgName imgExt] = fileparts(filepair{2});
     
        
		if (isempty(conf.saveExt))
			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
			'_' siS_.seedID '_landmarks.mat']; 
		else
			landmarks_SaveFile = [fullfile(conf.tempDir,imgDir,imgName)...
			'_' siS_.seedID '_landmarks_' conf.saveExt '.mat']; 
		end	
		load(landmarks_SaveFile,'Tseed','T','siS', 'tpointS');
        
        

		% Load image
		img = imread(fullfile(conf.imgDir,filepair{2}));
	    if ndims(img) < 3 % enforce RGB
                  img = repmat(img, [1 1 3]);
        end
		  % we dont want to use the saved T in the landmark file
         
        Tnew = newT{imgInd};
       

		% Compose average image
		
        congImg = mvpr_imcompose(size(avgImg), avgOrigo, img, Tnew);
		avgImg = avgImg+double(congImg);
		avgImgEye = avgImgEye+double(mvpr_imcompose(size(avgImg),avgOrigo,img,eye(3)));
        
%              %***********************************************************
%  newName =strcat('newcon_',num2str(imgInd));
%         
%         h = figure
%        imshow(avgImg);
%        saveas(h,newName,'jpg')
% %************************************************************************
        
        
        %%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
		if (conf.debugLevel >= 1)
			if (exist('dbgFig1') == false)
				clf;
				dbgFig1 = gcf;
				dbgAxis1 = axes('Parent',dbgFig1);
				dbgFig2 = figure;
				dbgAxis2 = axes('Parent',dbgFig2);
				dbgFig3 = figure;
				dbgAxis3 = axes('Parent',dbgFig3);
				dbgFig4 = figure;
				dbgAxis4 = axes('Parent',dbgFig4);
			end
		
			imagesc(img,'Parent',get(dbgFig1,'CurrentAxes'));
			set(dbgFig1,'ColorMap',colormap('gray'));
			title('Original image','Parent',get(dbgFig1,'CurrentAxes'));
			imagesc(congImg,'Parent',get(dbgFig2,'CurrentAxes'));
			set(dbgFig2,'ColorMap',colormap('gray'));
			title('Congealed image','Parent',get(dbgFig2,'CurrentAxes'));
			imagesc(avgImgEye,'Parent',get(dbgFig3,'CurrentAxes'));
			set(dbgFig3,'ColorMap',colormap('gray'));
			title('Average w/o congealing','Parent',get(dbgFig3,'CurrentAxes'));
			imagesc(avgImg,'Parent',get(dbgFig4,'CurrentAxes'));
			set(dbgFig4,'ColorMap',colormap('gray'));
			title('Average with congealing','Parent',get(dbgFig4,'CurrentAxes'));
			drawnow;
			if conf.debugInteractive
				input(' Press enter...');
            end
        end
    end
%         % only for the case when you want to save the transformed image one by one 
%         % ********************************************
%         avgImg = avgImg ./ (255*1);
%         newName =strcat('new_',num2str(imgInd));
%         pause(2)
%         h = figure
%        imshow(avgImg);
%        saveas(h,newName,'jpg')
%       
%         
%         %*********************************************************
    
    
end

avgImg = avgImg ./ (255*imgTot);

% Return image
imshow(avgImg);
title('Average image');
mvpr_lclose(fh);
hold off;
fh = figh;


