% Calculate Feature distance matrices
%
% o3d2d_autolm_feature_distancematrix_files(imgListFile, siS)
%
% Goes through the images in imgListFile and calculates full feature
% distance matrix for the seed image in seed image structure
% siS. Result matrices are stored to temporary save directory with
% full image paths and <IMGNAME>.fdistmatr.mat.
%
% Inputs:
%  imgListFile   - A list of images to be processed, one per line,
%                  containing no white spaces (this cuts the name) and
%                  with their full path (if 'imgDir' not specified).
%  siS           - Seed image structure.
%
% <Optional>
% 'tempSaveDir'  - Directory for temporary save items
%                  (e.g. extracted descriptors) (Def: '.')
% 'imgDir'       - Root directory for images (i.e. only relative
%                  paths given in imgFile_) (Def: '/')
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
% 'enforce'      - Enforce computing even if the file exists
%                  (Def. 1)
%
% Outputs:
%  -
%
% References:
%
% See also O3D2D_AUTOLM_SELECT_SEED_FILES
%
function o3d2d_autolm_feature_distancematrix_files(imgListFile, siS_, varargin)

%
% Parse input arguments
conf = struct('tempDir', 'TEMPWORK',...
              'imgDir', '.',...
              'featListFile',[],...
              'enforce', true,...
              'debugLevel', 0);
conf = mvpr_getargs(conf,varargin);

% The default is the same as in MVPR_FEATURE_EXTRACT_FILES
if isempty(conf.featListFile)
    [fpath fname fext] = fileparts(imgListFile);
    featListFile = fullfile(conf.tempDir,[fname '_feat' fext]);
else
    featListFile = conf.featListFile;
end;

if (~exist(featListFile))
    error(['List file ' featListFile 'containing images and extracted local'...
           'features not found - run mvp_feature_extract_files() for ' ...
           'your images first!']);
end;

%
% Count number of images
imgTot = mvpr_lcountentries(featListFile);

%
%% Open images (read the first line from the imgFile_)
fh = mvpr_lopen(featListFile, 'read');

timeTotalEstimated = 0;
timeTotalElapsed = 0;
timeElapsed = 0;
if conf.debugLevel > 0
	fprintf('Computing distance matrices\n');
end
%
% Go through images and form the distance matrices
for imgInd = 1:imgTot
	%fprintf('\r* Image (%5d/%5d) (%.2f / %.2f) seconds. ', imgInd, imgTot, time, totalTime);
	timeTotalEstimated = (timeTotalElapsed / (imgInd-1)) * (imgTot-imgInd);

	if conf.debugLevel > 0	
		fprintf('\r  * Progress: %5d/%5d, ETA: %.1fs  ', imgInd, imgTot, timeTotalEstimated);
	end

	tic;
	filepair = mvpr_lread(fh);

	% Test if distance matrix is already for computed with this seed
	[imgDir imgName imgExt] = fileparts(filepair{2});
	fdistmatrFile = [fullfile(conf.tempDir, imgDir, imgName)...
		         '_' siS_.seedID '_fdistmatr.mat'];

	if (exist(fdistmatrFile,'file') && (conf.enforce == false))
		load(fdistmatrFile,'fdistmatr');		
	else % distmatr doesn't exist or enforced -> create it
		% Load image features
                load(fullfile(conf.tempDir,filepair{3}),'descriptors','frames');

		% Compute distance matrix
		fdistmatr = inf * ones(size(siS_.descriptors, 2),size(descriptors, 2));
		fdistmatr = mvpr_feature_matchmatrix(siS_.descriptors, descriptors);			
				
		% Save all data
		siS = siS_; % dummy repeat
		save(fdistmatrFile,'fdistmatr','siS');
		
		
		% Debug plots / messages if required 
		% Requires level 1 debug mode        
		if (conf.debugLevel > 1) 
			if (exist('seedImg') == false)
				seedImg = imread(fullfile(conf.imgDir,siS_.seedImgFile));
			end
			otherImg = imread(fullfile(conf.imgDir, filepair{2}));
			debugPlot(seedImg, siS.frames, otherImg, frames, fdistmatr);
                        input(['N best matches using the descriptor ' ...
                               ' differences only <RET>']);
                end
	end
	timeElapsed = toc;
	timeTotalElapsed = timeTotalElapsed + timeElapsed;
end
mvpr_lclose(fh);
if conf.debugLevel > 0
	fprintf('\r  * Done! Total time elapsed: %.2fs, %.2fs per file.  \n', ...
	        timeTotalElapsed, timeTotalElapsed / imgTot);
end
end % end of function

function debugPlot(seedImg, seedFrames, otherImg, otherFrames, fdistmatr)
	% Create debug image
	width = size(seedImg,2) + size(otherImg,2);
	height = max([size(seedImg,1) size(otherImg,1)]);	
	dimensions = max([size(seedImg,3) size(otherImg,3)]);
	
	dbgImg = zeros(height, width, dimensions);
	dbgImg(1:size(seedImg,1), 1:size(seedImg,2),:) = seedImg;
	dbgImg(1:size(otherImg,1), 1+size(seedImg,2):width,:) = otherImg;
	
	% Create the view
	imshow(dbgImg/255);
	hold on;
	title('N best matches between the seed image and current image');
	set(gcf,'DoubleBuffer','on');

	[v idx] = sort(fdistmatr(:));
	dbgOff = size(seedImg,2);
	for n = 1:20
		[dbgi dbgj] = find(fdistmatr == v(n));
		seedFrame = seedFrames(:,dbgi);
		currFrame = otherFrames(:,dbgj);
	
		plot(seedFrame(1),seedFrame(2),'gd','MarkerSize',10,'LineWidth',2);
		plot(currFrame(1)+dbgOff,currFrame(2),'gd','MarkerSize',10,'LineWidth',2);
		plot([seedFrame(1) currFrame(1)+dbgOff],...
			[seedFrame(2) currFrame(2)]','g-','LineWidth',2);
		text(seedFrame(1),seedFrame(2),num2str(n),...
			'FontSize',14,'color','black','backgroundcolor','white');
		text(currFrame(1)+dbgOff,currFrame(2),num2str(n),...
			'FontSize',14,'color','black','backgroundcolor','white');
	end
	hold off;
	drawnow;
end
