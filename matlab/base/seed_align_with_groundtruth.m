% Calculates transform matrices for images with specified seed image using ground-truth
%
% [T scores] = o3d2d_autolm_seed_gt_align(seed, imgListFile, gtListFile) 
%  aligns all images in imgListFile to seedIdx using groundtruth filelist provided 
%  by gtListFile. Seed parameter is the index or filename in imgListFile.

function [T scores] = o3d2d_autolm_seed_gt_align(siS, imgListFile, gtListFile, varargin) 
%
% Parse input arguments
conf = struct('transform', 'similarity',...
              'matchRank', 5,...
              'iters', 1000,...
              'hitDist', 0.02,...
              'tempDir', 'temp/',...
              'imgDir', 'images/',...
	      'gtDir', 'gt',...
              'saveExt', [],...
              'debugLevel', 0, ...
              'usePixelDist', false,...
              'enforce',    true,...
              'retMatches', 1);

conf = mvpr_getargs(conf,varargin);


% Count number of images
imgTot = mvpr_lcountentries(imgListFile);

% timing
timeTotalEstimated = 0;
timeTotalElapsed = 0;
timeElapsed = 0;

% init
scores = {};
T = {};
% read seed points
gtSeedPoints = getSeedPoints(siS.seedImgNo, conf.gtDir, gtListFile);
fh = mvpr_lopen(gtListFile, 'read');
fh2 = mvpr_lopen(imgListFile, 'read');
for i = 1:imgTot
	gtFilename = mvpr_lread(fh);
	
	% Get points for the image
	if exist(fullfile(conf.gtDir,gtFilename{1}),'file')

          % Get points for the image
          gtPoints = load(fullfile(conf.gtDir,gtFilename{1}));
          gtNorm = norm(gtPoints(2, :) - gtPoints(5, :));
          
          % Transform points to the image point space
          gtT = mvpr_h2d_corresp(gtSeedPoints', gtPoints', 'hType', 'similarity');
          gtTPoints = mvpr_h2d_trans(gtSeedPoints', gtT)';
          % Calculate distance
          [a b c d] = calculateScore(gtPoints, gtTPoints);
          T{i} = gtT;
          scores{i} = (a / size(gtPoints,1)) / gtNorm;
        
          % DEBUG PICTURE
          if (conf.debugLevel > 1)
            fh2_lline = mvpr_lread(fh2);
            dbg_img = imread(fullfile(conf.imgDir, fh2_lline{2})); 
            imshow(dbg_img);
            hold on;
            plot(gtPoints(:,1),gtPoints(:,2),'go','LineWidth',2,'MarkerSize',10);
            hold off;
            input('DEBUG[2]: Image and landmarks <RET>');
          end;
        else
          warning(['File ' fullfile(conf.gtDir,gtFilename{1}) ...
                   ' not found for evaluation!']);
          scores{i} = inf;
        end;
end

mvpr_lclose(fh);
mvpr_lclose(fh2);

end % function




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function seedPoints = getSeedPoints(seed, dir, filename)


imgTot = mvpr_lcountentries(filename);
seedPoints = [];
seedIdx = 1;
if isnumeric(seed) % index
	if seed <= imgTot && seed > 0
		seedIdx = seed;
	end % if
else % filename
	% Find index
	fh = mvpr_lopen(filename, 'read');
	for i = 1:imgTot
		fline = mvpr_lread(fh);
		[a filebody c] = fileparts(fline{1});
		if (strcmp(filebody, seed))
			seedIdx = i;
			break;		
		end		
	end % for
	mvpr_lclose(fh);
end

fh = mvpr_lopen(filename, 'read');
for i = 1:seedIdx
	fline = mvpr_lread(fh);
end % for
mvpr_lclose(fh);

% Read file
seedPoints = load(fullfile(dir,fline{1}));

%imgTot = mvpr_lcountentries(filename);
%seedPoints = [];
%seedIdx = 1;
%if isnumeric(seed) % index
%	if seed <= imgTot && seed > 0
%		seedIdx = seed;
%	end % if
%else % filename
%	% Find index
%	fh = mvpr_lopen(filename, 'read');
%	for i = 1:imgTot
%		filename = mvpr_lread(fh);
%		[a filebody c] = fileparts(filename);
%		if (strcmp(filebody, seed))
%			seedIdx = i;
%			break;		
%		end		
%	end % for
%	mvpr_lclose(fh);
%end
%
%fh = mvpr_lopen(filename, 'read');
%for i = 1:seedIdx
%	filename = mvpr_lread(fh);
%end % for
%mvpr_lclose(fh);
%
%% Read file
%seedPoints = load(fullfile(dir,filename{1}));

end % function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [score_avg score_median score_min score_max] = calculateScore(pointsA, pointsB)
	score_max = 0;
	score_min = inf;
	score_avg = 0;
	distances = [];
	for p = 1:size(pointsA, 1)
		d = norm(pointsA(p,:) - pointsB(p,:));
		distances(p,:) = d;
		if d < score_min
			score_min = d;
		end
		if d > score_max
			score_max = d;
		end
		score_avg = score_avg + d;
	end
	score_avg = score_avg / size(pointsA, 1);

	sort(distances);
	median = floor(size(distances,1)/2);
	score_median = distances(median);
end % function
