function [ output_args ] = embed_images_inNodes( bg,nodeTotal,imgListFile,imgDir,bioType,pred)
% This function takes a handle to created graph, and
% finds nodes'position. Then puts axes in the nodes position to show the
% related image.

%inputs:
%          hg : a handle to biograph
%          nodeTotal : the total amount of nodes(images)
           

%output:  show images in the position of nodes of the generated graph


% By Fateme shokrollahi Yancheshmeh
%%
switch bioType
    case 'Graph'
%         set(bg,'LayoutType','equilibrium','EdgeType','straight',...
%      'ShowTextInNodes','None','NodeAutoSize','off');
 
    set(bg,'LayoutType','equilibrium','ShowTextInNodes','id','NodeAutoSize','off','ShowWeights','off');
      definedsize = [80 60];
    case 'Tree'
        set(bg,'LayoutType','radial','EdgeType','straight','ShowTextInNodes','None','NodeAutoSize','off','ShowWeights','off');
%  
       definedsize = [80 60];
       rootId =find(pred == 0);
       bg.nodes(rootId).LineWidth = 5;
       bg.nodes(rootId).Color =[0 1 0];
end
 
for i = 1:nodeTotal
    bg.nodes(i).Size =  definedsize;
end
 dolayout(bg);

 
 g = biograph.bggui(bg);
 hAx = g.biograph.hgAxes;

fh = figure('Units','Normalized','Position',...
    get(get(g.biograph.hgAxes,'Parent'),'Position'),'ActivePositionProperty','Position');

disp('--Maximize the biograph and press Enter--');
pause
copyobj(g.biograph.hgAxes,fh);
hold on,

seedline = mvpr_lopen(imgListFile, 'read');
for i = 1:nodeTotal
    
   s=bg.nodes(i).Size;
   nodePos =bg.nodes(i).Position;

   x=nodePos(1); %in points
   y=nodePos(2);
   wd=s(1);
   ht=s(2);
   seedImg =  mvpr_lread(seedline);
   seedDir= [fullfile(imgDir) seedImg{1}];
   img = imread(seedDir);

   
   % converting the position from points to normalized units, since
   % biogrpah uses 'points' Unit for nodes position, while figure's unit
   % and axes unit are in normalized .
   
   %NOTE:in ds2nfu function, if we dont specify a handle to the graphic
   %space like figure, plot,etc. it takes the last gca as the hAX, so here,
   %no biograph or other plotting function should be called between the
   %figure and ds2nfu.
   if i==1
       [pos notImportant Ax] = ds2nfu([x y wd ht]);
   else
       pos = ds2nfu(Ax,[x y wd ht]); 
   end
   
  a = axes;
  set(a,'position', [pos(1)-pos(3)/2, pos(2)-pos(4)/2, pos(3), pos(4)]);
  imshow(img);
   
end% for loop
if strcmp(bioType,'Tree')
    
    export_fig MST.png -m2.5
else
    export_fig graph.png -m2.5
end
%export_fig test3.png -native


mvpr_lclose(seedline);
%the following commands close the biograph.
child_handles = allchild(0);
names = get(child_handles,'Name');
k = find(strncmp('Biograph Viewer', names, 15));
% close(child_handles(k))

end

