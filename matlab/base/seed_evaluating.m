%inputs: 
%        SimilarityScore : matrix containing similarity values.
%        spatial_scoreMatrix : matrix consist of spatial scores.
%       
%outputs:
%        optseed : an integer value representing the seed NO in the
%        imagelistFile

% by Fateme Shokrollahi Yancheshmeh
%%
function [ optSeed ] = seed_evaluating( SimilarityScore, spatial_scoreMatrix,imgListFile)
% using spatial score and similarity values, selects one of the image as
% the best seed.


  imgResult = struct ('meanScore',0,...
    'stdScore',0,...
    'meanSim',0,...
    'stdSim',0,...
    'num',0,...
    'mean_std_ratio',0);

%vectore of  sructure
imgTot = mvpr_lcountentries(imgListFile);
for i= 1:imgTot
   
    imgResult(i).meanScore = mean(spatial_scoreMatrix(i,:));
    imgResult(i).stdScore = std(spatial_scoreMatrix(i,:));
    imgResult(i).mean_std_ratio =...
        imgResult(i).meanScore /  imgResult(i).stdScore;
    imgResult(i).meanSim =   mean(SimilarityScore(i,:));
    imgResult(i).stdSim =  std(SimilarityScore(i,:));
    imgResult(i).num = i;
   
end

tempVec = zeros(1,imgTot);
for i = 1:imgTot
    
       tempVec(i) =imgResult(i).meanScore;
       
end
mean_meanScore = mean(tempVec);
range= std(tempVec);
j = 0;
for i = 1:imgTot
    
    if  (imgResult(i).meanScore > mean_meanScore + range/2 | imgResult(i).meanScore == mean_meanScore + range/2)
        
        Filter();
   
    end 

end


tempVec = 0;
szT = size(temp);
if szT(2) ~= 0
    imgResult = [];
    imgResult = temp;
end
sz = size(imgResult);

for i = 1:sz(2)
  
   tempVec(i) = imgResult(i).mean_std_ratio;
     
end
mean_ratio = mean(tempVec);

temp = [];
 j= 0;
 
for i = 1:sz(2)
    
    if  (imgResult(i).mean_std_ratio > mean_ratio | imgResult(i).mean_std_ratio == mean_ratio )
        Filter();

    end   
end

tempVec = 0;
szT = size(temp);
if szT(2) ~= 0
    imgResult = [];
    imgResult = temp;
end
sz = size(imgResult);
for i = 1:sz(2)
   
    tempVec(i) = imgResult(i).meanSim;
   
end

mean_meanSim = mean(tempVec);
[min_meanSim id1] = min(tempVec);
tempVec = 0;
for i = 1:sz(2)
    
    tempVec(i) = imgResult(i).stdSim;
    
end

   j =0;
   temp = [];
   for i = 1:sz(2)
       
       if (imgResult(i).meanSim < mean_meanSim |  imgResult(i).meanSim == mean_meanSim)
           
           Filter();
           
       end
   end
  
 szT = size(temp);
if szT(2) ~= 0
    imgResult = [];
    imgResult = temp;
end
  temp = [];
  sz = size(imgResult);
  tempVec = 0;

  for i = 1:sz(2)
   
      tempVec(i) = imgResult(i).stdSim;
  end
  mean_stdSim = mean(tempVec);
  j = 0;
  for i = 1:sz(2)
       
       if (imgResult(i).stdSim < mean_stdSim | imgResult(i).stdSim == mean_stdSim)
           
           Filter();

       end
  end

szT = size(temp);
if szT(2) ~= 0
    imgResult = [];
    imgResult = temp;
end
 temp = [];
 sz= size (imgResult);
 tempVec = 0;
 for i = 1:sz(2)
     
     tempVec(i) = imgResult(i).meanSim;
     
 end

 [min_meanSim id] =min(tempVec);
 optSeed = imgResult(id);

 
    function Filter()
          
           j = j+1;
           temp(j).meanScore = imgResult(i).meanScore;
           temp(j).stdScore  = imgResult(i).stdScore;
           temp(j).mean_std_ratio = imgResult(i).mean_std_ratio ;
           temp(j).meanSim = imgResult(i).meanSim;
           temp(j).stdSim = imgResult(i).stdSim;
           temp(j).num = imgResult(i).num;
        
    end

end
    
           

