%the function is implemented for finding transformatin matrices for each
%link in shortest path found by Dijkstra algorithm or minmum spanning tree.

%INPUTs: 
%        transLink : is a cell aray including the sink and source number
%        for accuireing transform matrix(T{i}) of each link.


%OUTPUTS:
%        newT : new Transformation matrix from a candidate image to a seed,
%               the newT is like a multiplication of all transform matrices
%               during the shortest path from candid image(source) to the
%               seed.


% Author: by Fateme shokrollahi
function [ newT , stepT] = find_totalTransform( transLink,imgListFile,tempDir,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
fclose all
if (exist('mvpr_getargs') == 0)
	error(['MVPR matlab toolbox missing from the Matlab path'])
end

% Note that ip_method specific parameters set in the next step
conf = struct('seedMethod', 'forced',...
              'ip_method', 'vl_densems+vl_sift',... %'fs_hesaff+fs_sift',...
              'numOfLMs', [], ...
              'imgDir', '.',...
              'tempDir', 'TEMPWORK',...
              'transform','similarity',...
              'spatRansacIters', 200,...
              'lmRansacIters', 200,...
              'hitDistThreshold', [],...
              'lmMinDist', [],...
              'spatMatchRank', [],...
              'lmMatchRank', [],...
              'numOfLMMatches',1,...
	      'gtListFile', '[]',...
	      'gtDir', '.',...
	      'showResults', true,...
	      'showBBResults', true,...
	      'storeResults', false,...
              'enforceFeatureExtract', true,...
              'enforceFeaturePostProc', true,...
              'enforceSeedDistmatrices', true,...
              'enforceSpatScoreMatrices', true,...
              'enforceLMMatch', true,...
              'resThresholds', [0.05 0.10 0.25],...
              'useBB', false,...
              'bbDir', [],...
                'debugLevel', 0,...
              'spatMatchingMethod', 'RANSAC');
          


conf = mvpr_getargs(conf, varargin);

%%
%imgTot = mvpr_lcountentries(imgListFile);

T=[];
sz = size(transLink);
newT = 1;
 stepT= [];
for i = 1:sz(2)
    link = transLink{i};
    sinkId = link(1);
    sourceId = link(2);
   fh = mvpr_lopen(imgListFile, 'read');
   for n = 1:sinkId
       
   sinkpair  =  mvpr_lread(fh);

   end
       
    [dir sinkName ext] = fileparts(sinkpair{1});
    result =load_T_in_resultFile();
    load(result,'T');

    stepT{i} = T{sourceId};
 
    newT = newT * T{sourceId};

end
    
    
 %%   
function [resultFile] = load_T_in_resultFile()
switch conf.ip_method
    case 'vl_densems+vl_sift'
        fprintf(' (alignment: vl_demse,s+vl_sift) ');
        conf.hitDistThreshold = 0.04;
        conf.lmMinDist = 0.04; 
        conf.spatMatchRank = 5; 
        conf.lmMatchRank = 5; 
        conf.numOfLMs = 80; 
    otherwise
        fprintf([' (alignment: ' conf.ip_method ') ']);
        conf.hitDistThreshold = 0.02; 
        conf.lmMinDist = 0.02; 
        conf.spatMatchRank = 2; 
        conf.lmMatchRank = 2; 
        conf.numOfLMs = 20; 
end;
 
[imgListDir className ext] = fileparts(imgListFile);
     resultFile = [fullfile(tempDir, className)...
                  '-results'...
                  '-' sinkName '.' conf.ip_method ...
                  '.features'...
                  '-' num2str(conf.hitDistThreshold) ...
                  '-' num2str(conf.lmMinDist) ...
                  '-' num2str(conf.spatMatchRank) ...
                  '-' num2str(conf.numOfLMs) ...
                  '-' num2str(conf.spatRansacIters)];

 resultFile = [resultFile '.mat'];
if ~exist(resultFile) 
        disp('ERROR : the resultFile containing Transform matrices has not been produced!\n');
        disp('to have the resultFile, first run report02_demo2 with seedMethod=all\n');
        
 else
     fprintf('Using pre-computed data...\n');
 end
  
end
    
end

    