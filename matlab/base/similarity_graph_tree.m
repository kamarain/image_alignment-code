
%here we create a graph from similaritySCore.then the minimum spanning tree
% is created from the graph.

%input: 
%       similaritySCore: matrix in which elements represents the
%       similariy between seed and candidate images. the highest number is '1'
%       since after sorting the score matrix, the rank of the image wiht highest
%       score will be 50, and we have N=50, so according to this formula:
%       S(i,j)= N/rank(i,j), S(i,j) = Max(S(i,j),S(j,i)) 
%       50/50 = 1 which is actually the best match. the worst case is when
%       the rank of the image is "1" => Similarity= 50/1. so getting closed
%       to "1" means more similarity.

%output: 
%         Gsprs : Graph
%         tree : sparse matrix
%         pred : root of the tree

%By Fateme Shokrollahi Yancheshmeh
%%
function [ Gsprs , tree, pred] = similarity_graph_tree( similaritySCore,...
    imgListFile,imgDir,seed,GraphVisualize)

G = similaritySCore;
G_zeroDiag = G - diag(diag(G));
Gtrl = tril(G_zeroDiag);

Gsprs = sparse(Gtrl);
%full UNdirected graph with all of the neigbours
bg = biograph(Gsprs,[],'ShowArrows','off','ShowWeights','off');
set(bg,'LayoutType','equilibrium','EdgeType','straight');

dolayout(bg)

%///////////////////////////////////////////////////////////////////////
%the created Graph is so messy, so we just consider some of best maches for
%each node as its neighbours, then generate a DIRECTED Graph .

nbNeighbour = 3;  % you can set it, and it should be  <= total number of candidate images

sz = size(G);
temp(1:sz,1:sz)= sz(1)+10;
temp = diag(diag(temp));
temp = temp + G;

G_cut = zeros(sz);
edge_info(1:sz(1),1:nbNeighbour) = 0;

   for j = 1:sz(1)
       for counter = 1:nbNeighbour

          [minvalue minIndx] = min(temp(j,:));
          if minvalue > sz(1)
             break;
          else
            
           %saving the source node and sink nodes for 
           % illustration part, since we set different line width 
           %for each edge to show the distance of each sink from source.
           edge_info(j,counter) = minIndx;
           G_cut(j,minIndx) = minvalue;
           temp(j,minIndx) = sz(1) + 10;
          end
       end
   end

Graph_of_bestmatche(GraphVisualize,nbNeighbour,G_cut,edge_info,imgListFile,imgDir,sz(1));

%%
%creating Minimum spanning Tree
% tree is represented by a sparse matrix

        disp('--creating tree using SELECTED seed as root--');
        rootnum = seed;
        [tree,pred] = graphminspantree(Gsprs, rootnum);
    
bgtree = biograph(tree,[],'ShowArrows','on','ShowWeights','off');
if GraphVisualize
    embed_images_inNodes( bgtree,sz,imgListFile,imgDir ,'Tree',pred);
end


end


