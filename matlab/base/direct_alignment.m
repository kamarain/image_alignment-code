%DIRECT_ALIGNMENT Computes 2D transform matrices to align
%                        all images to the given seed image
%
% [T lScores fScores] = direct_alignment(seed, imgListFile) aligns all images
%              in imgListFile to the selected 'seed' image (index
%              in imgListFile or name). The function implements the
%              algorithm in ref. [1]
% 
% Please note that this function creates a temp directory in which it stores
% temporary data produced by various sub-functions. 
%
% Outputs:
%  T		 - Transform matrices for all images in imgListFile
%  lScores       - Landmark transformation scores (0..numOfLMs)
%                  i.e. num of matching landmarks.
%  fScores       - Landmark appearance scores, i.e. descriptor distances.
%
% Inputs:
%  seed              - Seed image as an index or basename of a file in imgList
%                      file.
%  imgListFile       - A list of images to be processed, one per line,
%                      containing no white spaces (this cuts the name) and
%                      with their full path (if 'imgDir' not specified).
%
% <Optional>
% 'seedMethod'       - Method used to selected (Def. 'forced')
%                       'forced' - Using the one given in the "seed" parameter.
%                       'random' - Randomly picks one
%                       'all'    - Runs every image as a seed (results saved)
% 'ip_method'        - Detector+descriptor pair to be used (as
%                      defined in mvpr_feature_extract()
%                      (def. 'fs_hesaff+fs_sift', found best in [1]).
% 'numOfLMs'         - Number of best landmarks selected (Defaults:
%                       vl_densems+vl_sift 80
%                       other              20 )
% 'imgDir'           - Root directory for images (i.e. only relative
%                      paths given in imgListFile) (Def: './')
% 'tempDir'          - Directory for temporary items
%                      (e.g. extracted descriptors) (Def: './TEMPWORK')
% 'transform'        - Selected 2D homography (see
%                      mvpr_h2d_corres() for options (def. 'similarity')
% 'spatRansacIters'  - Over this many random samples the spatial
%                      scores accumulated (Def. 200)
% 'lmRansacIters'    - Over this many random tests to select the
%                      landmarks between seed and each image (def. 200)
% 'hitDistThreshold' - Spatial score calculation threshold for
%                      normalised distance (proportional to seed
%                      image diagonal). (Defaults
%                       vl_densems+vl_sift 0.04
%                       other              0.02 )
% 'lmMinDist'        - Landmark distance threshold. No landmark is
%                      allowed if the distance to some other is
%                      less than this (defaults:
%                       vl_densems+vl_sift 0.04
%                       other              0.02 )
% 'spatMatchRank'    - Number of best candidates used in
%                      accumulation (defaults:
%                       vl_densems+vl_sift 5
%                       other              2 )
% 'lmMatchRank'      - Number of best candidates used in landmark
%                      selection (defaults:
%                       vl_densems+vl_sift 5
%                       other              2 )
% 'numOfLMMatches    - Number of best landmark combinations (object
%                      hypotheses) returned (def. 1)
% 'gtListFile'       - Similar to imgListFile, but contains list to
%                      files of manually annotated ground truth
%                      landmarks used to evaluate alignment (see
%                      ref. [1]) (Def. [])
% 'gtDir'            - Similar to imgDir, but for ground truth
%                      (Def. './').
% 'useBB'            - Use bounding boxes to filter features.
% 'bbDir'            - Bounding box directory (bounding boxes used
%                      to filter out local features if available.
% 'showResults'      - Show result images (average + accuracy
%                      graph) (Def. true)
% 'storeResults'     - Store the result images (needs export_fig
%                      package) (Def. false)
% 'enforceFeatureExtract'    - Def. true
% 'enforceFeaturePostProc'   - Def. true
% 'enforceSeedDistmatrices'  - Def. true
% 'enforceSpatScoreMatrices' - Def. true
% 'enforceLMMatch'           - Def. true
% 'resThresholds'            - If ground truth available, these
%                              will be used in evaluation (Def. [0.05 0.10 0.25])
% 'debugLevel'               - Debug level [0,1,2] (Def: 0 = none)
%
% References:
%
%  [1] J. Lankinen and J.-K. Kamarainen, Local Feature Based
%  Unsupervised Alignment of Object Class Images, British Machine
%  Vision Conference (BMVC2011).
%
% See also:
%
function [T lScores fScores res] = direct_alignment(seed, imgListFile, varargin)
%
% Check required functionality
if (exist('mvpr_getargs') == 0)
	error(['MVPR matlab toolbox missing from the Matlab path'])
end

% Note that ip_method specific parameters set in the next step
conf = struct('seedMethod', 'forced',...
              'ip_method', 'vl_densems+vl_sift',... %'fs_hesaff+fs_sift',...
              'numOfLMs', [], ...
              'imgDir', '.',...
              'tempDir', 'TEMPWORK',...
              'transform','similarity',...
              'spatRansacIters', 200,...
              'lmRansacIters', 200,...
              'hitDistThreshold', [],...
              'lmMinDist', [],...
              'spatMatchRank', [],...
              'lmMatchRank', [],...
              'numOfLMMatches',1,...
	      'gtListFile', '[]',...
	      'gtDir', '.',...
	      'showResults', true,...
	      'showBBResults', true,...
	      'storeResults', false,...
              'enforceFeatureExtract', true,...
              'enforceFeaturePostProc', true,...
              'enforceSeedDistmatrices', true,...
              'enforceSpatScoreMatrices', true,...
              'enforceLMMatch', true,...
              'resThresholds', [0.05 0.10 0.25],...
              'useBB', false,...
              'bbDir', [],...
              'debugLevel', 0,...
              'spatMatchingMethod', 'RANSAC');

conf = mvpr_getargs(conf, varargin);

%
switch conf.ip_method
 case 'vl_densems+vl_sift'
  fprintf(' (alignment: vl_demse,s+vl_sift) ');
  if isempty(conf.hitDistThreshold) conf.hitDistThreshold = 0.04; end;
  if isempty(conf.lmMinDist) conf.lmMinDist = 0.04; end;
  if isempty(conf.spatMatchRank) conf.spatMatchRank = 5; end;
  if isempty(conf.lmMatchRank) conf.lmMatchRank = 5; end;
  if isempty(conf.numOfLMs) conf.numOfLMs = 80; end;
 otherwise
  fprintf([' (alignment: ' conf.ip_method ') ']);
  if isempty(conf.hitDistThreshold) conf.hitDistThreshold = 0.02; end;
  if isempty(conf.lmMinDist) conf.lmMinDist = 0.02; end;
  if isempty(conf.spatMatchRank) conf.spatMatchRank = 2; end;
  if isempty(conf.lmMatchRank) conf.lmMatchRank = 2; end;
  if isempty(conf.numOfLMs) conf.numOfLMs = 20; end;
end;

% 1. Calculate image features
featListFile = mvpr_feature_extract_files_new(...
    imgListFile, ... 
    'imgDir', conf.imgDir, ...
    'tempDir', conf.tempDir, ...
    'method', conf.ip_method, ...
    'forceExtract', conf.enforceFeatureExtract,...
    'debugLevel', conf.debugLevel);

% 1.1 Post process the features
if conf.useBB
  fprintf(' post-processing with bounding boxes...\n');
  featListFile = ...
      o3d2d_autolm_bbmask_features_files(imgListFile,conf.imgDir, ...
                                         conf.bbDir, featListFile, conf.tempDir, ...
                                         'debugLevel', conf.debugLevel,...
                                         'enforce', conf.enforceFeaturePostProc);
  featsPostProcessed = true;
  fprintf(' Done!\n');
else
  featListFile = featListFile;
  featsPostProcessed = false;
end;

% all or some specific      
if (strcmp(conf.seedMethod,'all'))
  imgTot = 1:mvpr_lcountentries(imgListFile);
elseif (strcmp(conf.seedMethod,'rand50'))
  imgTot = randperm(mvpr_lcountentries(imgListFile));
  imgTot = imgTot(1:50);
elseif (strcmp(conf.seedMethod,'rand100'))
  imgTot = randperm(mvpr_lcountentries(imgListFile));
  imgTot = imgTot(1:100);
%elseif (strcmp(conf.seedMethod,'first10'))
%  imgTot = 1:10;
%elseif (strcmp(conf.seedMethod,'first50'))
%  imgTot = 1:50;
else
    imgTot = 1;
end

% Make a new feature list file since it drives what is used
if (strcmp(conf.seedMethod,'rand50') || strcmp(conf.seedMethod,'rand100'))
  maxInd = mvpr_lcountentries(featListFile);
  fprintf(' updating the file list using random N images (out of %d)...',maxInd);
  [lfDir lfFile lfExt] = fileparts(featListFile);
  new_featListFile = fullfile(lfDir,[lfFile '_randN' lfExt]);
  %[iDir iFile iExt] = fileparts(imgListFile);
  %new_imgListFile = fullfile(iDir,[iFile '_rand50' iExt]);
  if ~exist(new_featListFile,'file')
    fh_lf_new = mvpr_lopen(new_featListFile,'write');
    fh_lf = mvpr_lopen(featListFile,'read');
    for lf_ind = 1:maxInd
      lf_entry = mvpr_lread(fh_lf);
      if (~isempty(find(lf_ind == imgTot)))
        mvpr_lwrite(fh_lf_new,lf_entry)
      end;
    end;
    mvpr_lclose(fh_lf);
    mvpr_lclose(fh_lf_new);
  else
    fprintf('file exists, using that...')
  end;
  featListFile = new_featListFile;
  imgTot = 1:length(imgTot);
end;



res = squeeze(nan(length(imgTot),length(conf.resThresholds)));
for imgInd = 1:length(imgTot) % loop needed if all used as the seed one by one
    % 2. Create seed image structure
    if (strcmp(conf.seedMethod,'all'))
      fprintf('Testing all seeds (%4d / %4d)',imgTot(imgInd),length(imgTot));
      siS = direct_select_seed(...
          imgListFile, ...
          'method',	'forced', ...
          'forceSeed', imgTot(imgInd),...
          'imgDir',	conf.imgDir, ...
          'tempDir',	conf.tempDir, ...
          'featListFile', featListFile,...
          'debugLevel', conf.debugLevel);    
    elseif (strcmp(conf.seedMethod,'rand50') || strcmp(conf.seedMethod,'rand100'))
      fprintf('Testing with random N images (all as seeds) (%4d / %4d)',imgInd,length(imgTot));
      siS = direct_select_seed(...
          imgListFile, ...
          'method',	'forced', ...
          'forceSeed', imgTot(imgInd),...
          'imgDir',	conf.imgDir, ...
          'tempDir',	conf.tempDir, ...
          'featListFile', featListFile,...
          'debugLevel', conf.debugLevel);    
    elseif (strcmp(conf.seedMethod,'first10'))
      fprintf('Testing first 10 as seeds (%4d / %4d)',imgTot(imgInd),imgTot);
      siS = direct_select_seed(...
          imgListFile, ...
          'method',	'forced', ...
          'forceSeed', imgTot(imgInd),...
          'imgDir',	conf.imgDir, ...
          'tempDir',	conf.tempDir, ...
          'featListFile', featListFile,...
          'debugLevel', conf.debugLevel);    
    elseif (strcmp(conf.seedMethod,'first50'))
      fprintf('Testing first 50 as seeds (%4d / %4d)',imgTot(imgInd),imgTot);
      siS = direct_select_seed(...
          imgListFile, ...
          'method',	'forced', ...
          'forceSeed', imgTot(imgInd),...
          'imgDir',	conf.imgDir, ...
          'tempDir',	conf.tempDir, ...
          'featListFile', featListFile,...
          'debugLevel', conf.debugLevel);    
    else
        siS = direct_select_seed(...
            imgListFile, ...
            'method',	conf.seedMethod, ...
            'forceSeed', seed,...
            'imgDir',	conf.imgDir, ...
            'tempDir',	conf.tempDir, ...
            'featListFile', featListFile,...
            'debugLevel', conf.debugLevel);
    end;
    
    [imgDir imgFile imgExt] = fileparts(imgListFile);
    uniquePreamble = [fullfile(conf.tempDir, imgFile)...
                  '-results'...
                  '-' siS.seedID ...
                  '-' num2str(conf.hitDistThreshold) ...
                  '-' num2str(conf.lmMinDist) ...
                  '-' num2str(conf.spatMatchRank) ...
                  '-' num2str(conf.numOfLMs) ...
                  '-' num2str(conf.spatRansacIters)];
    resultFile = [uniquePreamble '.mat'];
%    [fullfile(conf.tempDir, imgFile)...
%                  '-results-' num2str(conf.spatRansacIters) ...
%                  '-' num2str(conf.hitDistThreshold) ...
%                  '-' num2str(conf.lmMinDist) ...
%                  '-' num2str(conf.numOfLMs) ...
%                  '-' num2str(conf.spatMatchRank) ...
%                  '-' siS.seedID '.mat'];
    
    if ~exist(resultFile) || conf.enforceSeedDistmatrices || ...
            conf.enforceSpatScoreMatrices || conf.enforceLMMatch
        % 3. Calculate feature distancematrices
        seed_distance_matrix(imgListFile, siS, ...
                                                  'imgDir',	conf.imgDir, ...
                                                  'tempDir',	conf.tempDir, ...
                                                  'enforce',	conf.enforceSeedDistmatrices, ...
                                                  'featListFile', featListFile,...
                                                  'debugLevel',	conf.debugLevel);
        
        % 4. Calculate spatialscorematrices
        [histMax, histSum, rScores] = ...
            seed_spatial_distance_matrix_files(...
                imgListFile, siS,...
                'imgDir',	 conf.imgDir, ...
                'hitDist',   conf.hitDistThreshold,...
                'matchRank', conf.spatMatchRank,...
                'iters',     conf.spatRansacIters,...
                'tempDir',	 conf.tempDir, ...
                'featListFile', featListFile,...
                'enforce',	 conf.enforceSpatScoreMatrices, ...
                'debugLevel',conf.debugLevel,...
                'spatMatchingMethod', conf.spatMatchingMethod);
        
        % 5. Select landmarks
        [lmIdx, lmP] = ...
            seed_select_landmarks(siS, histMax, conf.numOfLMs, ...
                                  'lmDist', 	conf.lmMinDist, ...
                                  'debugLevel', conf.debugLevel,...
                                  'dbg_imgDir', conf.imgDir);
        
        %% 6. Calculate alignments
        [T lScores fScores] = seed_align_with_landmarks(...
            imgListFile, siS, lmIdx, ...
            'imgDir',	conf.imgDir, ...
            'tempDir',	conf.tempDir, ...
            'hitDist',	conf.hitDistThreshold, ...
            'matchRank',	conf.lmMatchRank, ...
            'iters',        conf.lmRansacIters, ...
            'enforce',      conf.enforceLMMatch, ...
            'retMatches',   conf.numOfLMMatches,...
            'featListFile', featListFile,...
            'debugLevel',   conf.debugLevel);
        
        %% 7. Save data
        save(resultFile,'T','lmP','lmIdx','lScores','fScores');
        
    else
        fprintf('Using pre-computed data...\n');
        load(resultFile,'T','lmP','lmIdx', 'lScores','fScores');
    end % end of enforce
    
    %
    % Show average images
    if conf.showResults
      plotFile1 = [uniquePreamble '-avg_no_align.png'];
      plotFile2 = [uniquePreamble '-avg_aligned.png'];
      plotFile3 = [uniquePreamble '-graph.png'];
      datFile1 = [uniquePreamble '-graph.dat'];
      
      % A. Plot average
      %[fh avgImg] = o3d2d_autolm_plot_avg_files(imgListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir,'useCongealing', false, 'isVisible',false);
      [fh avgImg] = show_aligned_average_image(featListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir,'noAlignment', true, 'isVisible',false);
      if (conf.storeResults)
        plotfigh = figure(666); imshow(avgImg); set(plotfigh,'color','white');drawnow;
        export_fig(plotFile1);    
      end;
      %[fh avgImg2] = o3d2d_autolm_plot_avg_files(imgListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir, 'isVisible', false);
      [fh avgImg2] = show_aligned_average_image(featListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir, 'isVisible', false);
      if (conf.storeResults)
        plotfigh = figure(667); imshow(avgImg2); set(plotfigh,'color','white');drawnow;
        export_fig(plotFile2);
      end;
      
      set(fh,'visible','on');
      close(fh);
      totalImg = [avgImg avgImg2];
      figure(668); imshow(totalImg);
      title('Average image. Unaligned (left) and aligned (right)');
    end;
    
    % B. Evaluate landmars
    if ~isempty(conf.gtListFile)
      fprintf('Ground truth based evaluation executed!\n')
      %[lmT lmScores] = o3d2d_autolm_eval_landmarks(siS, T, imgListFile, conf.gtListFile, lmP(1:2,:), 'gtDir', conf.gtDir, 'tempDir', conf.tempDir);
      %[gtT gtScores] = o3d2d_autolm_seed_gt_align(siS, imgListFile, conf.gtListFile, 'gtDir', conf.gtDir, 'tempDir', conf.tempDir);
      [lmT lmScores] = eval_aligned_landmarks(siS, T, featListFile, conf.gtListFile, lmP(1:2,:), 'gtDir', conf.gtDir, 'tempDir', conf.tempDir);
      [gtT gtScores] = seed_align_with_groundtruth(siS, featListFile, conf.gtListFile, 'gtDir', conf.gtDir, 'tempDir', conf.tempDir,'imgDir',conf.imgDir,'debugLevel',conf.debugLevel);
      
      % Plot error curves
      if conf.showResults
        fh = figure(669);
        set(fh,'Visible', 'on'); hold on;
        plot(sort(cell2mat([lmScores; gtScores]),2),[0:(length(lmScores)-1)],'LineWidth',2); 
        legend('Automatic landmarks', 'Ground-truth');
        axis([0 0.5 0 length(lmScores)]);
        grid on;
        title('landmark error');
        xlabel('Error (relative)');
        ylabel('Number of fitted landmarks');
        if (conf.storeResults)
          set(fh,'color','white');drawnow;
          export_fig(plotFile3);
          plotFile3_data = sort(cell2mat([lmScores; gtScores]),2);
          plotFile3_data = [1:length(plotFile3_data); sort(cell2mat([lmScores; gtScores]),2)]';
          save(datFile1,'plotFile3_data','-ascii');
        end;
      end;

      resScores = sort(cell2mat([lmScores; gtScores]),2);
      fprintf('Images correctly aligned: ');
      for thind = 1:length(conf.resThresholds);
        if (max(squeeze(resScores(1,:))) <= conf.resThresholds(thind))
          res(imgInd, thind) = size(resScores,2);
        else
          res(imgInd, thind) = ...
              round(interp1(squeeze(resScores(1,:)),1:size(resScores,2),conf.resThresholds(thind),'linear'));
        end;
        fprintf('%3d for th=%1.3f, ', res(imgInd,thind), conf.resThresholds(thind));
      end;
      fprintf('done \n');
      
    else
      fprintf('Ground truth not available - no evaluation done!\n')
    end;

    % C. Evaluate bounding box overlap
    if ~isempty(conf.bbDir) && conf.showBBResults
      fprintf('Bounding box based evaluation executed!\n')
      if (strcmp(conf.seedMethod,'forced'))
        [bbOverlaps bbCoords] = ...
            eval_aligned_boundingbox(siS, T, imgListFile, featListFile, ...
                                     [1:mvpr_lcountentries(imgListFile)], ...
                                     'tempDir', conf.tempDir,'bbDir', ...
                                     conf.bbDir,'imgDir',conf.imgDir,...
                                     'debugLevel', conf.debugLevel);
        num_of_imgs = mvpr_lcountentries(imgListFile);
        bbox_fd = mvpr_lopen(imgListFile,'read');
        for img_ind = 1:num_of_imgs
          fd_str = mvpr_lread(bbox_fd);
          [save_p save_f save_e] = fileparts(fd_str{2});
          if ~exist(fullfile(conf.tempDir,save_p),'dir')
            mkdir(conf.tempDir,save_p); % make temp dir
          end;
          bbox_save = fullfile(conf.tempDir,save_p,[save_f '.box_est']);
          save_fid = fopen(bbox_save,'w');
          % minx maxx miny maxy format
          %fprintf(save_fid, '%.2f %.2f %.2f %.2f',...
          %        bbCoords(img_ind,1), bbCoords(img_ind,2),...
          %        bbCoords(img_ind,3), bbCoords(img_ind,4));
          % all four coordinates format
          fprintf(save_fid, '%.2f %.2f\n',...
                  bbCoords(img_ind,3), bbCoords(img_ind,1));
          fprintf(save_fid, '%.2f %.2f\n',...
                  bbCoords(img_ind,4), bbCoords(img_ind,1));
          fprintf(save_fid, '%.2f %.2f\n',...
                  bbCoords(img_ind,4), bbCoords(img_ind,2));
          fprintf(save_fid, '%.2f %.2f',...
                  bbCoords(img_ind,3), bbCoords(img_ind,2));
          fclose(save_fid);
        end;
        mvpr_lclose(bbox_fd);
      else
        [bbOverlaps] = ...
            eval_aligned_boundingbox(siS, T, imgListFile, featListFile, ...
                                     imgTot,'tempDir', conf.tempDir,'bbDir', ...
                                     conf.bbDir,'imgDir',conf.imgDir,...
                                     'debugLevel', conf.debugLevel);
      end;
      bbScore(imgInd) = mean(bbOverlaps);
      bbSeed{imgInd} = siS.seedImgFile;
      fprintf('Average overlap %0.2f for the seed %s ',...
              bbScore(imgInd),bbSeed{imgInd});
      fprintf('done \n');
    else
      fprintf('Bounding boxes not available - no evaluation done!\n')
    end;

    % clear all images - though, this still makes images blink and
    % follow your virtual screen which is very annoying (imshow()
    % should be replaced with image()?)
    if strcmp(conf.seedMethod,'all')
      foo = findall(0);
      if sum(foo == 666) clf(666); end;
      if sum(foo == 667) clf(667); end;
      if sum(foo == 668) clf(668); end;
      if sum(foo == 669) clf(669); end;
      % close all is shit as it produces blinking figure which
      % follow virtual screens (you cannot work anymore)
      %close all; % otherwise images will write over again and again
    end;
    
end; % for imgInd = 1:imgTot
    
