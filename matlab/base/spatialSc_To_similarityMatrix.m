% spatial_similarityMatrix_tree()
% this functin is implemented based on the provided inforamtion in [1].

% using the scores provided in spatial_scorematrix, and utelizing this
% formula :s(i,j)=N/rank(i,j) . s(i,j) = max(s(i,j),s(j,i)) [1]

% input:  
%           imglistfile: a text file containing the name 
%           and directory of images
%           
%           spatial_scoreMatrix : matrix including spatial scores
%           
%           imgDir : image directory
          
% output:  
%           similarity : similarity matrix
%           indx :      creating similarity matrix requires sorting score 
%                       Matrix, therefor the place of each element of score
%                       matrix will be changed, while the column number of 
%                       the elements shows the image number in
%                       imagelistfile. Therefore,'indx' contains the column
%                       number(imagenumber) of each element in the 
%                       similarity matrix.


% Reference:
%[1] Unsupervised Visual Alignment with Similarity Graphs.(CVPR 2015)


% Author: Fatemeh Shokrollahi Yancheshmeh

%%
function [ similarity, indx ] = spatialSc_To_similarityMatrix(imgListFile,...
    spatial_scoreMatrix, imgDir )

imgTot = mvpr_lcountentries(imgListFile);
N = imgTot;

similarity = zeros(imgTot);
[sortedScores indx] = sort(spatial_scoreMatrix,2);
s = zeros(imgTot);

%Note: for choosing best, based on the score, we should ignore the score
%between seed image with itself! so the best match, will be located on
%columnn 50, for N=50. there are also other images having 80 scores that
%might locate on the last column, but in some cases the seed is located on
%the last column! so if it happens then the column 49 should be considered
%as best match.


seedline = mvpr_lopen(imgListFile, 'read');
 for i = 1:imgTot % seed no
     for j = 1:imgTot
         
         rank(i,j) = find(indx(i,:) == j);
         s(i,j) = N/rank(i,j);
          
     end
    
     seedImg =  mvpr_lread(seedline);
     seedDir= [fullfile(imgDir) seedImg{1}];
    
     BestMatch(i).no = indx(i,imgTot);
     if BestMatch(i).no == i
         BestMatch(i).no = indx(i,imgTot-1);
     end
      
     fh = mvpr_lopen(imgListFile, 'read');
     for line = 1:BestMatch(i).no
         
         pair = mvpr_lread(fh);
          
     end
     
     BestMatch(i).pair = pair{1};
     BestMatch(i).Dir = [fullfile(imgDir) BestMatch(i).pair];
     [classtype BestMatch(i).name z] = fileparts(pair{1});
     mvpr_lclose(fh);
     
 end% end of  first FOR loop
 
 mvpr_lclose(seedline);

 for i = 1:imgTot 
     for j = 1:imgTot
         
         similarity(i,j) = max(s(i,j),s(j,i));

     end
 end

end

