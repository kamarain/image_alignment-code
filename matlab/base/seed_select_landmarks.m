% Select landmarks with spatialscorehistogram
%
% [landmarkIndices, landmarkPoints] =
%           o3d2d_autolm_select_landmarks(siS, spatialScoreHist, N)
%
% Using the spatial scores in spatialScoreHist selects N best
% landmarks.
%
% Inputs:
%  siS               - Seed image structure.
%  spatialScoreHist  - Spatial score histogram for all seed features
%                      over all images.
%  N                 - Number of best landmarks.
%
% <Optional>
% 'scoreOrient'  - Score orientation for spatScoreHist ('max':
%                  largest is the best, 'min': smallest is the
%                  best) (Def. 'max'). 
% 'lmDist'       - Landmark minimum distance from each other. Prevents 
%                  "stacking". (Def. 0.02)
% 'debugLevel'   - Debug level [0,1,2] (Def: 0)
%
% Outputs:
%  landmarkIndices   - 1 x n Index numbers of the selected seed
%                      features (see siS_) 
%  landmarkPoints    - 2 x n Coordinates of the selected seeds
%  
%  Note that number of landmarks returned can be less than N because the number
%  of features may be low or lmDist parameter filters too many points.
%
% References:
%
% See also O3D2D_AUTOLM_FEATURE_SPATIALSCOREMATRIX_FILES
%
function [seedLmInds,seedLmCoords] = ...
	seed_select_landmarks(siS_,spatScoreHist_,n_,varargin)

%
% Parse input arguments
conf = struct('scoreOrient', 'max',...
              'lmDist', 0.05, ...
	      'debugLevel', 0, ...
	      'dbg_imgDir', '.', ...
	      'usePixelDist', false);
conf = mvpr_getargs(conf,varargin);

%
% Load score histogram to select best landmarks
if strmatch(conf.scoreOrient,'max')
	[foo allseedLmInds] = sort(spatScoreHist_,'descend');
else
	[foo allseedLmInds] = sort(spatScoreHist_,'ascend');
end;

%
% Initialize variables
nNotSelected = true; 
currN = 0;
itern = 0;
seedLmInds = [];

% Loop until requested number of landmarks from requested
% distance to each other found => seedLmCoords
while (nNotSelected)
	itern = itern + 1;
	candSeed = allseedLmInds(itern); 
	candCoords = siS_.frames(:,[seedLmInds candSeed]);
	siS_.frames(:,seedLmInds);

	%if ((checkuniqueness(candCoords(1:2,:),conf.usePixelDist, lmDist_, siS_.norm) || ...
	if ((check_distances(candCoords(1:2,:), conf.lmDist, 'usePixelDistance', conf.usePixelDist, 'distanceNorm', siS_.norm) || ...
	    length(candCoords) == 1) && itern <= size(siS_.frames,2))
		seedLmInds = [seedLmInds candSeed];
		seedLmCoords = siS_.frames(:,seedLmInds);
		currN = currN + 1;
	end
	
	if (itern >= size(siS_.frames,2))
		warning(['Your distance threshold is too strict - not ' ...
			'requested number of landmarks found but only ' num2str(currN)]);
		nNotSelected = false;
		continue;
	end
	if (currN == n_)
		nNotSelected = false;
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% DEBUG [1] BEGIN %%%%%%%%%%
if (conf.debugLevel > 1)

	if (exist('seedImg') == false)
		seedImg = imread(fullfile(conf.dbg_imgDir,siS_.seedImgFile));
	end;
	cla reset;
	imshow(seedImg);
	title([num2str(currN) ' best landmarks']);
	hold on;
	set(gcf,'DoubleBuffer','on');
	for foo = 1:currN
		plot(seedLmCoords(1,foo),seedLmCoords(2,foo),'yd','MarkerSize',10,'LineWidth',2);
		text(seedLmCoords(1,foo),seedLmCoords(2,foo),num2str(foo),...
			'FontSize',12,'color','yellow','backgroundcolor','black');
	end;
	drawnow
	hold off;
end

end % end of function
%%%%%%%%% DEBUG [1] END   %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  INTERNAL FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
%
%function [u] = checkuniqueness(pointCoords_, usePixelDist_, uDist_, uNorm_)

%u = true;
%for fooind = 1:size(pointCoords_,2)
%	fooCoords = pointCoords_;
%	fooCoords(:,fooind) = realmax;
%	fooDists = fooCoords-repmat(pointCoords_(:,fooind),[1 size(fooCoords,2)]);
%	if usePixelDist_
%		fooDists = sqrt(sum(fooDists.^2,1));
%	else
%		fooDists = sqrt(sum(fooDists.^2,1)) ./ uNorm_;
%	end
%	if (sum(fooDists < uDist_) > 0)
%		u = false;
%	end;
%end;


