function [ ] = align(method,bestSeed,pred,shortpathType, newT,imgListFile, varargin)%
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
% Note that ip_method specific parameters set in the next step
% Check required functionality

fclose all;


if (exist('mvpr_getargs') == 0)
	error(['MVPR matlab toolbox missing from the Matlab path'])
end
conf = struct('seedMethod', 'forced',...
              'ip_method', 'vl_densems+vl_sift',... %'fs_hesaff+fs_sift',...
              'numOfLMs', [], ...
              'imgDir', '.',...
              'tempDir', 'TEMPWORK',...
              'transform','similarity',...
              'spatRansacIters', 200,...
              'lmRansacIters', 200,...
              'hitDistThreshold', [],...
              'lmMinDist', [],...
              'spatMatchRank', [],...
              'lmMatchRank', [],...
              'numOfLMMatches',1,...
	          'gtListFile', '[]',...
	          'gtDir', '.',...
	          'showResults', true,...
	          'showBBResults', true,...
	          'storeResults', false,...
              'enforceFeatureExtract', true,...
              'enforceFeaturePostProc', true,...
              'enforceSeedDistmatrices', true,...
              'enforceSpatScoreMatrices', true,...
              'enforceLMMatch', true,...
              'resThresholds', [0.05 0.10 0.25],...
              'useBB', false,...
              'bbDir', [],...
              'debugLevel', 0,...
              'exp',[],...
              'spatMatchingMethod', 'RANSAC');

          


conf = mvpr_getargs(conf, varargin);
switch conf.ip_method
 case 'vl_densems+vl_sift'
  fprintf(' (alignment: vl_demse,s+vl_sift) ');
  if isempty(conf.hitDistThreshold) conf.hitDistThreshold = 0.04; end;
  if isempty(conf.lmMinDist) conf.lmMinDist = 0.04; end;
  if isempty(conf.spatMatchRank) conf.spatMatchRank = 5; end;
  if isempty(conf.lmMatchRank) conf.lmMatchRank = 5; end;
  if isempty(conf.numOfLMs) conf.numOfLMs = 80; end;
 otherwise
  fprintf([' (alignment: ' conf.ip_method ') ']);
  if isempty(conf.hitDistThreshold) conf.hitDistThreshold = 0.02; end;
  if isempty(conf.lmMinDist) conf.lmMinDist = 0.02; end;
  if isempty(conf.spatMatchRank) conf.spatMatchRank = 2; end;
  if isempty(conf.lmMatchRank) conf.lmMatchRank = 2; end;
  if isempty(conf.numOfLMs) conf.numOfLMs = 20; end;
end;

 

  
    

%% 1. Calculate image features
featListFile = mvpr_feature_extract_files_new(...
    imgListFile, ... 
    'imgDir', conf.imgDir, ...
    'tempDir', conf.tempDir, ...
    'method', conf.ip_method, ...
    'forceExtract', conf.enforceFeatureExtract,...
    'debugLevel', conf.debugLevel);

% 1.1 Post process the features
if conf.useBB
  fprintf(' post-processing with bounding boxes...\n');
  featListFile = ...
      o3d2d_autolm_bbmask_features_files(imgListFile,conf.imgDir, ...
                                         conf.bbDir, featListFile, conf.tempDir, ...
                                         'debugLevel', conf.debugLevel,...
                                         'enforce', conf.enforceFeaturePostProc);
  featsPostProcessed = true;
  fprintf(' Done!\n');
else
  featListFile = featListFile;
  featsPostProcessed = false;
end;
% all or some specific      
if (strcmp(conf.seedMethod,'all'))
  imgTot = 1:mvpr_lcountentries(imgListFile);
elseif (strcmp(conf.seedMethod,'rand50'))
  imgTot = randperm(mvpr_lcountentries(imgListFile));
  imgTot = imgTot(1:50);
elseif (strcmp(conf.seedMethod,'first10'))
  imgTot = 1:10;
elseif (strcmp(conf.seedMethod,'first50'))
  imgTot = 1:50;
else
    imgTot = 1;
end
%%
% Make a new feature list file since it drives what is used
if (strcmp(conf.seedMethod,'rand50'))
  maxInd = mvpr_lcountentries(featListFile);
  fprintf(' updating the file list using random 50 images (out of %d)...',maxInd);
  [lfDir lfFile lfExt] = fileparts(featListFile);
  new_featListFile = fullfile(lfDir,[lfFile '_rand50' lfExt]);
  if ~exist(new_featListFile,'file')
    fh_lf_new = mvpr_lopen(new_featListFile,'write');
    fh_lf = mvpr_lopen(featListFile,'read');
    for lf_ind = 1:maxInd
      lf_entry = mvpr_lread(fh_lf);
      if (~isempty(find(lf_ind == imgTot)))
        mvpr_lwrite(fh_lf_new,lf_entry);
      end;
    end;
    mvpr_lclose(fh_lf);
    mvpr_lclose(fh_lf_new);
  else
    fprintf('file exists, using that...')
  end;
  featListFile = new_featListFile;
  imgTot = 1:50;
end;

if (strcmp(method,'Direct'))
    bestSeed = find(pred == 0);% *
end

res = squeeze(nan(length(imgTot),length(conf.resThresholds)));
%% 2. Create seed image structure
 siS = direct_select_seed(...
          imgListFile, ...
          'method',	'forced', ...
          'forceSeed', bestSeed,... % bestSeedNO
          'imgDir',	conf.imgDir, ...
          'tempDir',	conf.tempDir, ...
          'featListFile', featListFile,...
          'debugLevel', conf.debugLevel); 
      
  [imgDir imgFile imgExt] = fileparts(imgListFile);
    uniquePreamble = [fullfile(conf.tempDir, imgFile)...
                  '-results'...
                  '-' siS.seedID ...
                  '-' num2str(conf.hitDistThreshold) ...
                  '-' num2str(conf.lmMinDist) ...
                  '-' num2str(conf.spatMatchRank) ...
                  '-' num2str(conf.numOfLMs) ...
                  '-' num2str(conf.spatRansacIters)];
    resultFile = [uniquePreamble '.mat'];

    
    if ~exist(resultFile) || conf.enforceSeedDistmatrices || ...
            conf.enforceSpatScoreMatrices || conf.enforceLMMatch
        
        fprintf('--Warning, there is on resultFile, Run report2_demo2 first--');
        
    else
        fprintf('Using pre-computed data...\n');
        if (strcmp(method,'Direct'))
            load(resultFile,'T','lmP','lmIdx', 'lScores','fScores');
        else
            load(resultFile,'lmP','lmIdx', 'lScores','fScores');
        end
    end % end of enforce
    
%% B. Evaluate landmars
    if ~isempty(conf.gtListFile)
      fprintf('Ground truth based evaluation executed!\n')
      
      [gtT , gtScores] = seed_align_with_groundtruth(siS, featListFile, conf.gtListFile, 'gtDir', conf.gtDir, 'tempDir', conf.tempDir,'imgDir',conf.imgDir,'debugLevel',conf.debugLevel);
      
      
      if (strcmp(method,'Direct'))
          [lmT , lmScores{1}] = eval_aligned_landmarks(siS, T, featListFile, conf.gtListFile, lmP(1:2,:), 'gtDir', conf.gtDir, 'tempDir', conf.tempDir);
          find_total_correct_aligned(1);
      else
          for i= 1:size(newT,2)
              [lmT , lmScores{i}] = eval_aligned_landmarks(siS, newT{i}, featListFile, conf.gtListFile, lmP(1:2,:), 'gtDir', conf.gtDir, 'tempDir', conf.tempDir);
              num_of_correct{i} = find_total_correct_aligned(i);
            
          end
          
      end
       
    else
      fprintf('Ground truth not available - no evaluation done!\n')
    end;
    %% Show average images and error curves
   if conf.showResults
      plotFile1 = [uniquePreamble '-avg_no_align.png'];
      plotFile2 = [uniquePreamble '-avg_aligned.png'];
      plotFile3 = [uniquePreamble '-graph.png']; 
      switch method 
          case 'Direct'
              [fh avgImg] = show_aligned_average_image(featListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir,'noAlignment', true, 'isVisible',false);
              if (conf.storeResults)
                  plotfigh = figure(666); imshow(avgImg); set(plotfigh,'color','white');drawnow;
                  export_fig(plotFile1);    
              end
              fprintf('----alignment with  root---')
              [fh avgImg2] = show_aligned_average_image(featListFile, siS, lScores, 'tempDir', conf.tempDir, 'imgDir', conf.imgDir, 'isVisible', false);
              if (conf.storeResults)
                  plotfigh = figure(667); imshow(avgImg2); set(plotfigh,'color','white');drawnow;
                  export_fig(plotFile2);
              end
              set(fh,'visible','on');
              close(fh);
              totalImg = [avgImg avgImg2];
              figure(668); imshow(totalImg);
              title('Average image. Unaligned (left) and aligned (right)');
              error_curve(1,4);
                  
       
          case 'StepWise'
              switch conf.exp
                  case 'StepWise VS Direct'
                      %compare 
                       tmp = cell2mat(num_of_correct); 
                       if tmp(1)>tmp(4)
                               tmpInd=1;
                       elseif (tmp(1)==tmp(4)) && (tmp(2)>tmp(5))
                               tmpInd=1; 
                       elseif (tmp(1)==tmp(4)) && (tmp(2)==tmp(5))&& tmp(3)>tmp(6)
                               tmpInd=1;
                       else
                               tmpInd=2;
                       end  
                       plot_avg(tmpInd,3);
                       error_curve(tmpInd,3);
             
                  case 'Dijkstra VS Direct'
                       plot_avg(1,2);
                       error_curve(1,2)
                      
                  case 'MST VS Direct'
                       plot_avg(1,1);
                       error_curve(1,1)
                      
                  case 'MST vs Dijkstra'
                       plot_avg(1,2);%Dijkstra
                       error_curve(1,2);
                      
                       plot_avg(2,1);%MST
                       error_curve(2,1)
                    
              end
             
           
      end
     
    end;
   %%
   %Find total number of correctly aligned images
    function [num]= find_total_correct_aligned(id)
        resScores = sort(cell2mat([lmScores{id}; gtScores]),2);
        j=1;
        for thind = 1:length(conf.resThresholds);
            if (max(squeeze(resScores(1,:))) <= conf.resThresholds(thind))
                res(bestSeed, thind) = size(resScores,2);
            else
               res(bestSeed, thind) = ...
               round(interp1(squeeze(resScores(1,:)),1:size(resScores,2),conf.resThresholds(thind),'linear'));
            end
        num(j)= res(bestSeed, thind);
        j=j+1;
    
       
        end
        
       
    end
%%
%plot average of aligned images
    function plot_avg(Tid,titID)
        tit = {'MST','Dijkstra','stepWise'};
        [fh avgImg] = show_avgImages_shortestpathVersion(featListFile,siS,newT{Tid},'tempDir', conf.tempDir, 'imgDir', conf.imgDir,'noAlignment', true, 'isVisible',false);
            if (conf.storeResults)
                plotfigh = figure(700); imshow(avgImg); set(plotfigh,'color','white');drawnow;
                export_fig(plotFile1);    
            end;
            [fh avgImg2] = show_avgImages_shortestpathVersion(featListFile,siS,newT{Tid},'tempDir', conf.tempDir, 'imgDir', conf.imgDir, 'isVisible', false);
            if (conf.storeResults)
                plotfigh = figure(701); imshow(avgImg2); set(plotfigh,'color','white');drawnow;
                export_fig(plotFile2);
            end;
            set(fh,'visible','on');
            close(fh);
            totalImg = [avgImg avgImg2];
            if (strcmp(tit{titID},'MST'))
                 figure(703); imshow(totalImg);
            else
                figure(702); imshow(totalImg);
            end
           
            title(strcat(tit{titID},'__','Average image. Unaligned (left) and aligned (right)'));
        
    end
%%
%plot error curves
function error_curve(imID,titID)
    
        tit = {'MST','Dijkstra','stepWise','Direct'};
        if (strcmp(method,'Direct'))
               fh = figure(669);
        elseif (strcmp(tit{titID},'MST'))
               fh = figure(704);
        elseif (strcmp(tit{titID},'Dijkstra'))
                  fh=figure(705);
        else
              fh = figure(703);
        end
        set(fh,'Visible', 'on');
        hold on;
        plot(sort(cell2mat([lmScores{imID}; gtScores]),2),[0:(length(lmScores{imID})-1)],'LineWidth',2); 
        legend('Automatic landmarks', 'Ground-truth');
        axis([0 0.5 0 length(lmScores{imID})]);
        grid on;
        title(strcat( tit{titID},'__','landmark error'));
  
        
        xlabel('Error (relative)');
        ylabel('Number of fitted landmarks');
        if (conf.storeResults)
          set(fh,'color','white');drawnow;
          export_fig(plotFile3);
        end;
end
    
end%