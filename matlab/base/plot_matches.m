% Plot matches between images
%
% fh = o3d2d_autolm_plot_matches(img, frames, matches)
%    Plot feature matches for images, loads images (img) as cell structure where each
%    cell is a single
%
function fh = plot_matches(img, frames, matches, varargin)

% Parse input arguments
conf = struct('handle',[],...
              'visible', 'on',...
	      'labels',[]);
conf = mvpr_getargs(conf,varargin);

% validatate input
if ~iscell(img) % not a cell -> only one image
	img = {img};
	frames = {frames};
	matches = matches;
end
if size(img,2) > 2
	error('Too many input images');
end
F = frames{1};
for l = 1:size(F,2)
	a = F(3,l);
	b = F(4,l);
	c = F(5,l);
	
	[v e] = eig([a b;b c]);
	
	l1 = 1/sqrt(e(1));
	l2 = 1/sqrt(e(4));
	F(3,l) = l1*v(1);
	F(4,l) = l1*v(2);
	
	F(5,l) = l2*v(3);
	F(6,l) = l2*v(4);
end
frames{1} = F;

if size(frames,2) == 2
	F = frames{2};
	for l = 1:size(F,2)
		a = F(3,l);
		b = F(4,l);
		c = F(5,l);
			
		[v e] = eig([a b;b c]);
		
		l1 = 1/sqrt(e(1));
		l2 = 1/sqrt(e(4));
		F(3,l) = l1*v(1);
		F(4,l) = l1*v(2);
		
		F(5,l) = l2*v(3);
		F(6,l) = l2*v(4);
	end
	frames{2} = F;
end
% If there are 2 images
if size(img, 2) == 2 
	if size(matches, 1) < 2
		error('Not enough matching points');
	end
	if size(img{2},1) > size(img{1},1)
		sizeY = size(img{2},1);
	else
		sizeY = size(img{1},1);
	end
	sizeX = (size(img{1},2) + size(img{2},2));
	sizeZ = size(img{1},3);
	
	mImg = zeros(sizeY, sizeX, sizeZ);
	
	mImg(1:size(img{1},1), 1:size(img{1},2),1:sizeZ) = img{1};
	mImg(1:size(img{2},1), size(img{1},2):(size(img{1},2)-1+size(img{2},2)),:) = img{2};
	mImg = uint8(mImg);
else
	% Create image
	mImg = img{1};	
end

% Plot image
% Create figure
if conf.handle
	fh = figure(conf.handle); %,'Visible', conf.visible);
else
	fh = figure('Visible', conf.visible);
end
imshow(mImg);
hold on;

% Plot frames
vl_plotframe( frames{1}(:,matches(1,:)),'LineWidth',2 );

if size(conf.labels,1) > 0
	for x = 1:size(matches,2)
		text(frames{1}(1,matches(1,x)),frames{1}(2,matches(1,x)), ...
		     num2str(conf.labels(x,:)'),...
		     'BackgroundColor',[1.0 1.0 .0],...
		     'EdgeColor','black');
	end
end

% second image
if size(img, 2) == 2 
	
	% move points
	fs = frames{2}(:,matches(2,:));
	fs(1,:) = fs(1,:) + size(img{1},2);
	
	% plot frames
	vl_plotframe(fs, 'LineWidth', 2);
	
	for i = 1:size(matches,2)
		line = [frames{1}(1,matches(1,i)) frames{1}(2,matches(1,i)); ...
	        size(img{1},2)+frames{2}(1,matches(2,i)) frames{2}(2,matches(2,i))];
		plot(line(:,1), line(:,2),'b-','MarkerSize',10,'LineWidth',3);
	end
end
%drawnow;
hold off;
end

