% the Dijkstra algorithm is used to find the shortest weighted paths from a
% source node to sink in a graph.
%the algorithm uses a data structure Priority Queue which itslef implemented
%by Heap.
%Dijkstra uses also the algorithm RELAX. the relaxation of the edge (u,v)
%tests if the shortest path from the vertex v can be improved by routing
%its end through u and does so if necessary.

%INPUTE:  
%       Source: containes the vertex'x number in the graph as the starting point. 
%              
%       seedId: vertex'x number in the graph represeting our destination.
%       graph:  matrix containing the weight of each edge

%OUTPUT:
%       path: vector including visited vertices in shortest path from source 
%             to the final destination.
%       transLink: cell array containing vectros with the source and seed
%       number for each link to make the finding transformatin matrix
%       easier. e.g [seedID sourceId] so we need to find T (transform) from source to
%       seed, and we can find it by loading resultFile.

% By Fateme Shokrollahi Yancheshmeh
%%

function [transLink path] = Dijkstra(source,seedId,graph )
% the Dijkstra algorithm is used to find the shortest weighted paths from a
% source node to sink in a graph

% in the begining the fields of each vertex are color = white, d=inf,
% pi=NIL

% an empty priority queue, elements with lowest distance has higher
% priority

Q = struct('color',[],'num',[],'distance',[],'pi',[]);
sz= size(graph);
weight = graph;
imgTot = sz(2);
colorSet = {'white','gray','black'};
for i= 1 :imgTot
    vertex(i).color='white'; 
    vertex(i).num = i;

   %if the vertex has been discoverd its distance from 'S', else Inf
   vertex(i).distance = Inf;

   % pi = a pointer to the vertex through which v was found the first time,
   % NIL for undiscovered  vertices.
   vertex(i).pi = NaN;
end
path= [];
transLink = [];
% an empty priority queue. elements with lowest distance has higher
% priority
Q(1)=[];

%make the source found by changing its color
vertex(source).color = 'gray';
vertex(source).num = source;
vertex(source).distance = 0;
vertex(source).pi = NaN;

%push the source into priority Queue
push_Queue(source); %////////////////
[~, Qsize] =size(Q);
while Qsize ~= 0  %continue while there are vertices left
    u = extract_Min();%////
    if u == seedId
        shortPath(u);
        break;
    else
       %find the neighbors of u
       u_adj = find(graph(u,:));
       adjSize = size(u_adj);
       for j = 1:adjSize(2)
           
          if u_adj(j) ~= u
              if (strcmp(vertex(u_adj(j)).color,'white'))
                  vertex(u_adj(j)).color ='gray';
                  push_Queue(u_adj(j));
              end
              Relax();
          end
       end       
      vertex(u). color = 'black';
       
       
    end
    [~, Qsize] =size(Q);
end
%%
function Relax()
    
  if vertex(u_adj(j)).distance > vertex(u).distance + weight(u,u_adj(j))
     vertex(u_adj(j)).distance = vertex(u).distance + weight(u,u_adj(j));
     vertex(u_adj(j)).pi = u;
     update_vertex_inQ(u_adj(j));
  end
  
end
%%
    function update_vertex_inQ(vId)
        
        [~, ~, val] = (arrayfun(@(x)find(x.num),Q));
        [~,Qid] = find(val == vId);
        Q(Qid).distance = vertex(vId).distance;
        Q(Qid).pi = vertex(vId).pi;
        
    end
%%
    function shortPath(node)
   
          n = 0;
          while ~(isnan(vertex(node).pi))
               n = n + 1;
              path(n) = node;
              node = vertex(node).pi;
              
          end
          path(n+1) = node;
          sinkTot = size(path);
          in = 1;
          for sinkNum = sinkTot(2) : -1 : 2
              sourceId = path(sinkNum);
              sinkId = path(sinkNum-1);
              transLink{in} = [sinkId sourceId];
              transLink{in};
              in = in + 1;
          end
          
    end

%%
  function push_Queue(id)
     
      tail = size(Q);
      tail = tail(2)+1;
      Q(tail) = vertex(id);
      
      
  end
 %%
    function [verId]= extract_Min()
       
        [minval,idx] = min(arrayfun(@(x)min(x.distance),Q));
        verId = Q(idx).num;
        pop_Queue(idx);
      
    end
%%
  function pop_Queue(qid)
      
%        [sz1 sz2]=size(Q);
%         if ((sz1 == 1) && (sz2 == 1))
%             Q = [];
%         else
            Q(qid) = [];
%         end
      
    end
        
end    
    

    

