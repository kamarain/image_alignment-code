%IMAGE_ALIGNMENT_DEMO1
%
% This demo aligns a set of images using auxiliary ("privileged")
% user provided information, such as bounding boxes. The
% configuration file (IMAGE_ALIGNMENT_DEMO1_CONF.M) contains
% pre-configurations for the selected datasets:
%
%  * Caltech-101 (bounding boxes)
%  * r-Caltech-101 (bounding boxes)^1
%  * ImageNet (bounding boxes)
%
%  ^1 Bounding boxes not provided so they are generated
%     using user annotations in Caltech-101 and r-Caltech-101
%
% NOTE: code contains special processing of the bounding box
% formats used by different datasets that is fragile to changes!
%
% Examples:
%  See: https://bitbucket.org/kamarain/image_alignment-code/wiki/Home
%
% See also IMAGE_ALIGNMENT_DEMO1_CONF.M and MVPR_SUMIMG.M .
%
% Authors:
%  Jukka Lankinen
%  Joni Kamarainen
%
% References:
%  [1] J. Lankinen and J.-K. Kamarainen, Local Feature Based
%  Unsupervised Alignment of Object Class Images, British Machine
%  Vision Conference (BMVC2011).

fprintf('-------------------------------------------------\n');
fprintf('             IMAGE_ALIGNMENT_DEMO1               \n');
fprintf(' Alignment with user data                        \n');
fprintf('-------------------------------------------------\n');

conf_file = './image_alignment_demo1_conf';
fprintf(['=> Loading parameters from ' conf_file])
run(conf_file);
fprintf('   - Done!\n');

imgTot = mvpr_lcountentries(conf.imgListFile);
fh = mvpr_lopen(conf.imgListFile, 'read');

sumImg = mvpr_sumimg([],[],[]);

if (imgTot > conf.max_images)
  fprintf(['=> Too many images (' num2str(imgTot) ...
           ') using only ' num2str(conf.max_images) '\n']);
  imgTot = conf.max_images;
end;

for ii = 1:imgTot
    filepair = mvpr_lread(fh);
    img = imread(fullfile(conf.imgDir,filepair{1}));

    if conf.useBB==true && conf.BBIsWrong == false
      [bb_dir bb_fname bb_ext] = fileparts(filepair{2});
      if strcmp(bb_ext,'.xml')
        xmin = [];
        bb_xml = xml2struct(fullfile(conf.bbDir,filepair{2}));
        for node_ind = 1:length(bb_xml.Children)
          if strcmp(bb_xml.Children(node_ind).Name,'object')
            bb_xml = bb_xml.Children(node_ind);
            break;
          end
        end;
        for node_ind = 1:length(bb_xml.Children)
          if strcmp(bb_xml.Children(node_ind).Name,'bndbox')
            bb_xml = bb_xml.Children(node_ind);
            break;
          end
        end;
        for node_ind = 1:length(bb_xml.Children)
          if strcmp(bb_xml.Children(node_ind).Name,'xmin')
            xmin = str2num(bb_xml.Children(node_ind).Children.Data);
          end
          if strcmp(bb_xml.Children(node_ind).Name,'ymin')
            ymin = str2num(bb_xml.Children(node_ind).Children.Data);
          end
          if strcmp(bb_xml.Children(node_ind).Name,'xmax')
            xmax = str2num(bb_xml.Children(node_ind).Children.Data);
          end
          if strcmp(bb_xml.Children(node_ind).Name,'ymax')
            ymax = str2num(bb_xml.Children(node_ind).Children.Data);
          end
        end;
        if isempty(xmin)
          error(['XML parsing failed for ' ...
                 fullfile(conf.bbDir,filepair{2}) ...
                 '!'])
        end;
        box_coord = [ymin ymax xmin xmax];
      
      else 
        bb = load(fullfile(conf.bbDir,filepair{2}));
        
        if isfield(bb,'box_coord')
          box_coord = bb.box_coord;
          if size(box_coord,1) == 4 && size(box_coord,2) == 2
            % our own format for the rotated box coords
            true_box_coord = box_coord;
            box_coord = ...
                [min([box_coord(:,2)]) max(box_coord(:,2)) ...
                 min(box_coord(:,1)) max(box_coord(:,1))];
          end;
          
        else % assuming full box corners given as row: tl, tr, br, bl
             % form minY maxY minX maxX
          box_coord = [bb(1,2) bb(3,2) bb(1,1) bb(3,1)];
        end;
      end;
        
        cimg = imcrop(img,[box_coord(3) box_coord(1)...
                           box_coord(4)-box_coord(3) box_coord(2)-box_coord(1)]);
        
        %%%%%%%% DEBUG 1 START %%%%%%
        if conf.debugLevel > 0
            % Plot image and the bounding box
            clf;
            subplot(1,2,1);
            imagesc(img);
            hold on;
            % rectangle clock-wise from the upper left corner
            plot([box_coord(3) box_coord(4) box_coord(4) box_coord(3) box_coord(3)],...
                 [box_coord(1) box_coord(1) box_coord(2) box_coord(2) box_coord(1)],...
                 'r-')
            if exist('true_box_coord')
              plot([true_box_coord(:,1); true_box_coord(1,1)],...
                   [true_box_coord(:,2); true_box_coord(1,2)],'y--');
            end;
            hold off;
            box_coord
            
            subplot(1,2,2);
            imagesc(cimg);
            input('Image and bounding box (left) and a cropped image <RET>')
        end;        
        %%%%%%%% DEBUG 1 END  %%%%%%

        sumImg = mvpr_sumimg(sumImg,cimg,[]);
        
    elseif conf.useBB==true && conf.BBIsWrong == true
        lms = load(fullfile(conf.annoDir,filepair{3}));
        orig_lms = load(fullfile(conf.orig_annoDir,filepair{4}));
        bb = load(fullfile(conf.bbDir,filepair{2}));

        T = mvpr_h2d_corresp(orig_lms',lms','hType','similarity');
        boxcorners = ...
            [bb.box_coord(3) bb.box_coord(4) bb.box_coord(4) bb.box_coord(3) bb.box_coord(3);...
             bb.box_coord(1) bb.box_coord(1) bb.box_coord(2) bb.box_coord(2) bb.box_coord(1)];
        boxcorners_T = mvpr_h2d_trans(boxcorners,T);
        minx = min(boxcorners_T(1,:));
        maxx = max(boxcorners_T(1,:));
        miny = min(boxcorners_T(2,:));
        maxy = max(boxcorners_T(2,:));
        n_boxcorners_T = [minx maxx maxx minx minx;...
                          miny miny maxy maxy miny];
        
        cimg = imcrop(img,[max(minx,1) max(miny,1) min(size(img,2)-minx,maxx-minx) min(size(img,1)-miny,maxy-miny)]);

        %%%%%%%% DEBUG 1 START %%%%%%
        if conf.debugLevel > 0
            % Plot image and the bounding box
            clf;
            subplot(1,2,1);
            imagesc(img);
            hold on;
            % rectangle clock-wise from the upper left corner
            plot(boxcorners_T(1,:),boxcorners_T(2,:),'g-');
            plot(n_boxcorners_T(1,:),n_boxcorners_T(2,:),'r-');
            hold off;
            
            subplot(1,2,2);
            imagesc(cimg);
            input('Image and bounding box (left) and a cropped image <RET>')
        end;        
        %%%%%%%% DEBUG 1 END  %%%%%%

        sumImg = mvpr_sumimg(sumImg,cimg,[]);

    elseif conf.useBB==false
        %%%%%%%% DEBUG 1 START %%%%%%
        if conf.debugLevel > 0
            % Plot image and the bounding box
            clf;
            imagesc(img);
            input(sprintf('Image %3d/%3d <RET>',ii,imgTot));
        end;        
        %%%%%%%% DEBUG 1 END  %%%%%%
        sumImg = mvpr_sumimg(sumImg,img,[]);
    end;
end;

mvpr_lclose(fh);

clf
imshow(uint8(sumImg/imgTot));
%colormap gray;
axis off
set(gcf,'color','white')
set(gcf,'InvertHardcopy','off');
disp('Average (sum) image over the set.');
