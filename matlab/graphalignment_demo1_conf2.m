% This is a config script for  fatememsc_demo1.m

% NOTE: this file requires to first run the image_align_simGraph_requirement.m
%       in order to have this type of files
%       '<IMG>_<SEEDIMG>_<FEATTYPE>_landmarks.mat'
%        in TEMPWORK including information such as spatial
%       score, seedID, etc.

%%
fprintf('---------------------------------------------\n');
fprintf('Object3d2d unsupervised imagalignment demo \n');
fprintf('---------------------------------------------\n');
fprintf('-- Loading parameters from graphalignment_demo1_conf1... --')
run('./graphalignment_demo1_conf1');
fprintf('-- ...Done --\n');
% total number of images in the listfile
imgTot = mvpr_lcountentries(conf.imgListFile);  
%initialising score matrix
spatial_scoreMatrix = zeros(imgTot);


%fh = mvpr_lopen(conf.imgListFile), 'read');

%here we set a pointer at the begining of the file and read each line to
%findout the imageID. In the first loop, we pick up each line as query and
%in the iner loop, we set a pair for the query as candidate which should goes
%through all images in the list.

%searching for intended files in TEMPWORK/class/...
% and filling score matrix.


qLine = mvpr_lopen((conf.imgListFile), 'read');
for i = 1:imgTot
    filepair1 = mvpr_lread(qLine);
    [qClass queryImg typ] = fileparts( filepair1{1} );
    conf.class = qClass;
    query.name = queryImg;
    cLine = mvpr_lopen(conf.imgListFile, 'read');
      for j = 1:imgTot
      
          % candidate image 
          filepair2 = mvpr_lread(cLine);
          [cClass candidImg typ] = fileparts( filepair2{1} );
          candidate.name = candidImg;
          landmarks_SavedFile = [fullfile(conf.tempDir,conf.class,candidate.name)...
		'_' query.name '.' conf.ip_method '.features_landmarks.mat']; 
          load(landmarks_SavedFile)
          spatial_scoreMatrix(i,j) = scores;
      end 
      mvpr_lclose(cLine);

end 
 mvpr_lclose(qLine);
    
        
   
    




