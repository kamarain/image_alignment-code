%IMAGE_ALIGNMENT_DEMO2
%
% This demo runs the unsupervised alignment algorithm described in
% ref. [1] and plots qualitative results (average images) and
% quantitative results (errors to the the manually annotated
% landmarks). This demo can be used to test how different parameter
% values affect to the results.
%
% The configuration file (IMAGE_ALIGNMENT_DEMO2_CONF.M) contains
% pre-configurations for the selected datasets: 
%
%  * Caltech-101 (bounding boxes)
%  * r-Caltech-101 (bounding boxes)^1
%  * ImageNet (bounding boxes)
%
%  ^1 Bounding boxes not provided so they are generated
%     using user annotations in Caltech-101 and r-Caltech-101
%
% Examples:
%  See: https://bitbucket.org/kamarain/image_alignment-code/wiki/Home
%
% See also IMAGE_ALIGNMENT_DEMO1.M, IMAGE_ALIGNMENT_DEMO2_CONF.M,
% MVPR_FEATURE_EXTRACT_NEW.M .
%
% Authors:
%  Jukka Lankinen
%  Joni Kamarainen
%
% References:
%  [1] J. Lankinen and J.-K. Kamarainen, Local Feature Based
%  Unsupervised Alignment of Object Class Images, British Machine
%  Vision Conference (BMVC2011).

fprintf('-------------------------------------------------\n');
fprintf('             IMAGE_ALIGNMENT_DEMO2               \n');
fprintf(' Alignment using the direct method               \n');
fprintf('-------------------------------------------------\n');

conf_file = './image_alignment_demo2_conf';
fprintf(['=> Loading parameters from ' conf_file])
run(conf_file);
fprintf('   - Done!\n');

fprintf('-- Running alignment --\n')
[a b c] = direct_alignment(conf.seed, conf.imgListFile, ...
                           'seedMethod',conf.seedMethod,...
                           'imgDir', conf.imgDir, ...
                           'tempDir', conf.tempDir,...
                           'enforceFeatureExtract', conf.enforceFeatureExtract,...
                           'enforceFeaturePostProc', conf.enforceFeaturePostProc,...
                           'enforceSeedDistmatrices',conf.enforceSeedDistmatrices,...
                           'enforceSpatScoreMatrices',conf.enforceSpatScoreMatrices,...
                           'enforceLMMatch', conf.enforceLMMatch,...
                           'gtDir', conf.gtDir, ...
                           'gtListFile', conf.gtListFile,...
                           'debugLevel', conf.debugLevel,...
                           'showResults', conf.showResults,...
                           'showBBResults', conf.showBBResults,...
                           'storeResults', conf.storeResults,...
                           'ip_method', conf.ip_method,...
                           'useBB', conf.useBB,...
                           'bbDir',conf.bbDir,...
                           'spatMatchingMethod', conf.spatMatchingMethod);
fprintf('-- ...Done --\n');
