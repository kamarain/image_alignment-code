% This is a config script for ivc_demo2.m - make your own copy
%


% Select a dataset and its class
classes = {'r-caltech101-stop_sign',... %1
           'r-caltech101-faces',...     %2
           'r-caltech101-motorbikes',...%3
           'r-caltech101-carside',...   %4
           'caltech101-airplanes',...   %5
           'caltech101-starfish',...    %6
           'ImageNet-meerkat',...       %7
           'LFW5000'};                  %8
classno = 2; % one of above (1-8)

experiment={'StepWise VS Direct',...  % Figure.3 in CVPR2015 papre
            'MST vs Dijkstra',...     % Figure.4 in CVPR2015 paper
            'Dijkstra VS Direct',...  %Figure.5 in CVPR2015 paper      
            'MST VS Direct' };   
expNo=2; %one of above (1-4)
%%
switch classes{classno}
    case 'r-caltech101-stop_sign'
        fprintf(' (using pre-configuration for the class ''r-caltech101-stop_sign'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'r-caltech101/101_ObjectCategories/';
        conf.gtDir  = 'r-caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_stop_sign_50.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_stop_sign_50_gt.txt';
        conf.ip_method = 'fs_hesaff+fs_sift';
        
    case 'r-caltech101-faces'
        fprintf(' (using pre-configuration for the class ''r-caltech101-faces'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'r-caltech101/101_ObjectCategories/';
        conf.gtDir  = 'r-caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_Faces_easy_48_rcaltech101.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_Faces_easy_48_gt_r.txt';
        %conf.ip_method = 'fs_hesaff+fs_sift';
        conf.ip_method = 'vl_densems+vl_sift'; % default
      %conf.imgListFile = 'data_demos_cvpr2015/filelist_Faces_easy_50.txt';
      %conf.gtListFile = 'data_demos_cvpr2015/filelist_Faces_easy_50_gt_r.txt';
        
    case 'r-caltech101-motorbikes'
        fprintf(' (using pre-configuration for the class ''r-caltech101-motorbikes'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'r-caltech101/101_ObjectCategories/';
        conf.gtDir  = 'r-caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_Motorbikes_chopper_50.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_Motorbikes_chopper_50_gt.txt';
        conf.ip_method = 'fs_hesaff+fs_sift';
    
    case 'r-caltech101-carside'
        fprintf(' (using pre-configuration for the class ''r-caltech101-carside'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'r-caltech101/101_ObjectCategories/';
        conf.gtDir  = 'r-caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_car_side_selected_50.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_car_side_selected_50_gt.txt';
        conf.ip_method = 'fs_hesaff+fs_sift';
    
    case 'caltech101-airplanes'
        fprintf(' (using pre-configuration for the class ''r-caltech101-airplanes'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'caltech101/101_ObjectCategories/';
        conf.gtDir  = 'caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_airplanes_50.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_airplanes_50_gt.txt';
        %conf.ip_method = 'fs_hesaff+fs_sift';
        conf.ip_method = 'vl_densems+vl_sift'; % default
        
    case 'caltech101-starfish'
        fprintf(' (using pre-configuration for the class ''r-caltech101-starfish'')');
        conf.tempDir = 'TEMPWORK_rcaltech101';
        conf.imgDir  = 'caltech101/101_ObjectCategories/';
        conf.gtDir  = 'caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_starfish_50.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_starfish_50_gt.txt';  
        %conf.ip_method = 'fs_hesaff+fs_sift';
        conf.ip_method = 'vl_densems+vl_sift'; % default
        
    case 'ImageNet-meerkat'
        fprintf(' (using pre-configuration for the class ''ImageNet-meerkat'')');
        conf.tempDir = 'TEMPWORK_ImageNet';
        conf.imgDir  = 'ImageNet/';
        conf.gtDir  = '/ImageNet/landmark';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_meerkat.txt';
        conf.gtListFile = 'data_demos_cvpr2015/filelist_meerkat_gt.txt';  
        conf.ip_method = 'fs_hesaff+fs_sift';
        %conf.ip_method = 'vl_densems+vl_sift'; 
        
    case 'LFW5000'
        fprintf(' (using pre-configuration for the class ''LFW5000'')');
        conf.tempDir = 'TEMPWORK_LFW';
        conf.imgDir  = 'lfw-2200-1000';
        conf.gtDir  = 'r-caltech101/landmarks/';
        conf.imgListFile = 'data_demos_cvpr2015/filelist_starfish_50.txt';
        conf.gtListFile = '';  
%        conf.imgListFile ='report02_data/lfw5000.txt'
%        conf.gtListFile = [];
%      

    otherwise,
    error('Unknown class selected!');
end;%switch

%%
% Set the method specific parameters
conf.seedMethod = 'all'
conf.spatMatchingMethod = 'RANSAC'; % RANSAC/fminsearch
conf.showResults = true; % important for seedMethod all
conf.showBBResults = false;
conf.storeResults = false; % important for seedMethod all
conf.enforceFeatureExtract = false;
conf.enforceFeaturePostProc = false;
conf.enforceSeedDistmatrices = false;
conf.enforceSpatScoreMatrices = false;
conf.enforceLMMatch = false;
conf.debugLevel = 1;
conf.useBB = false;
conf.exp = experiment{expNo};
conf.GraphVisualize = false; %set it true for graph and MST visualisation
                             %Note: Biograph library of matlab is needed.
conf.seed = [];
conf.bbDir = [];

 
 
